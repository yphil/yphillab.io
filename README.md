[![website](https://img.shields.io/website?down_message=down&up_color=brightgreen&up_message=up&url=https%3A%2F%2Fyphil.gitlab.io%2Fblog)](https://yphil.gitlab.io/blog)
[![Liberapay](https://img.shields.io/liberapay/receives/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Liberapay](https://img.shields.io/liberapay/goal/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Ko-Fi](https://img.shields.io/badge/Ko--fi-F16061?label=buy+me+a&style=flat&logo=ko-fi&logoColor=white)](https://ko-fi.com/yphil/tiers)

---
Source files of [my blog](https://yphil.gitlab.io/)
----

[ci]: https://about.gitlab.com/gitlab-ci/
[hexo]: https://hexo.io
[install]: https://hexo.io/docs/index.html#Installation
[documentation]: https://hexo.io/docs/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[hexo-server]: https://hexo.io/docs/server.html
