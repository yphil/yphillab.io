/**
 * Sets up Justified Gallery.
 */
if (!!$.prototype.justifiedGallery) {
  var options = {
    rowHeight: 140,
    margins: 4,
    lastRow: "justify"
  };
  $(".article-gallery").justifiedGallery(options);
}

$(document).ready(function() {

    function setTheme(theme) {
        let selectors = 'h1, .h1, h2, .h2, h3, .h3, .content p, .toc-item, .pagination span';
        if (theme === 'dark') {
            $('.highlight').css({'background-color': '#111', 'color': '#999'});
            $('.highlight .name').css('color', '#7c7');
            $('.highlight .string').css('color', '#cc7');
            $('.code .keyword').css('color', '#ccf');
            $('.function_').css('color', '#fbf');
            $('.post .content a, .pagination a').css('color', 'darkorange');
            $(selectors).css('color', '#999');
            $('body').css({'background-color': '#333', 'color': '#ccc'});
        } else if (theme === 'light') {
            $('body').css({'background-color': '#eee', 'color': '#222'});
            $(selectors).css('color', '#222');
        }
    }

    function setFont(font) {
        const fontValue = font === 'serif' ? 'Serif' : 'Sans-serif';
        $('.post p, .post p a, article ol, article ul').css('font-family', fontValue);
    }

    const storedTheme = localStorage.theme || 'light';
    const storedFont = localStorage.font || 'sans';

    setTheme(storedTheme);
    setFont(storedFont);

    $('a[href="#theme-switcher"]').text(storedTheme === 'dark' ? 'Light ☀️' : 'Dark 🌖');
    $('a[href="#font-switcher"]').text(storedFont === 'serif' ? 'Sans' : 'Serif');

    $('a[href="#theme-switcher"]').click(function () {
        const newTheme = localStorage.theme === 'light' ? 'dark' : 'light';
        setTheme(newTheme);
        localStorage.theme = newTheme;
        $(this).text(newTheme === 'dark' ? 'Light ☀️' : 'Dark 🌖');
    });

    $('a[href="#font-switcher"]').click(function () {
        const newFont = localStorage.font === 'sans' ? 'serif' : 'sans';
        setFont(newFont);
        localStorage.font = newFont;
        $(this).text(newFont === 'serif' ? 'Sans' : 'Serif');
    });

  /**
   * Shows the responsive navigation menu on mobile.
   */
  $("#header > #nav > ul > .icon").click(function() {
    $("#header > #nav > ul").toggleClass("responsive");
  });


  /**
   * Controls the different versions of  the menu in blog post articles
   * for Desktop, tablet and mobile.
   */
  if ($(".post").length) {
    var menu = $("#menu");
    var nav = $("#menu > #nav");
    var menuIcon = $("#menu-icon, #menu-icon-tablet");

    /**
     * Display the menu on hi-res laptops and desktops.
     */
    if ($(document).width() >= 1440) {
      menu.show();
      menuIcon.addClass("active");
    }

    /**
     * Display the menu if the menu icon is clicked.
     */
    menuIcon.click(function() {
      if (menu.is(":hidden")) {
        menu.show();
        menuIcon.addClass("active");
      } else {
        menu.hide();
        menuIcon.removeClass("active");
      }
      return false;
    });

    if (menu.is(":hidden")) {
      menu.show();
      menuIcon.addClass("active");
    }

    /**
     * Add a scroll listener to the menu to hide/show the navigation links.
     */
    if (menu.length) {
      $(window).on("scroll", function() {
        var topDistance = menu.offset().top;

        // hide only the navigation links on desktop
        if (!nav.is(":visible") && topDistance < 50) {
          nav.show();
        } else if (nav.is(":visible") && topDistance > 100) {
          nav.hide();
        }

        // on tablet, hide the navigation icon as well and show a "scroll to top
        // icon" instead
        if ( ! $( "#menu-icon" ).is(":visible") && topDistance < 50 ) {
          $("#menu-icon-tablet").show();
          $("#top-icon-tablet").hide();
        } else if (! $( "#menu-icon" ).is(":visible") && topDistance > 100) {
          $("#menu-icon-tablet").hide();
          $("#top-icon-tablet").show();
        }
      });
    }

    /**
     * Show mobile navigation menu after scrolling upwards,
     * hide it again after scrolling downwards.
     */
    if ($( "#footer-post").length) {
      var lastScrollTop = 0;
      $(window).on("scroll", function() {
        var topDistance = $(window).scrollTop();

        if (topDistance > lastScrollTop){
          // downscroll -> show menu
          $("#footer-post").hide();
        } else {
          // upscroll -> hide menu
          $("#footer-post").show();
        }
        lastScrollTop = topDistance;

        // close all submenu"s on scroll
        $("#nav-footer").hide();
        $("#toc-footer").hide();
        $("#share-footer").hide();

        // show a "navigation" icon when close to the top of the page,
        // otherwise show a "scroll to the top" icon
        if (topDistance < 50) {
          $("#actions-footer > #top").hide();
        } else if (topDistance > 100) {
          $("#actions-footer > #top").show();
        }
      });
    }
  }
});
