---
title: About me
date: 2022-10-11 21:53:20
---

- ✉️ [Contact](https://twitter.com/yPhilCode)
My Twitter account, since I got **banned** from Mastodon 😃
- `</>` [Code](https://gitlab.com/users/yphil/projects) / [Code](https://framagit.org/yphil)
My projects @Gitlab
- 🎹 [Music](https://exode.me/c/yphil_music) ([YouTube channels](https://www.youtube.com/@yPhil))
My albums & singles, radio shows, etc
- 🎸 [Demos](https://soundcloud.com/yphil)
My music demos (might move soon)
- 🚸 [Tutorials](https://exode.me/a/yphil/)
My tutorial videos
- ❤️ [Participate](http://liberapay.com/yphil)
Reccuring donations [@Liberapay](http://liberapay.com/yphil), reccuring & one-time donations [@ko-fi](https://ko-fi.com/yphil/tiers)
- ⚙️ [Help others](http://stackoverflow.com/users/1729094)
My Q&As @StackOverflow

[![](https://pbs.twimg.com/profile_banners/72038930/1476879387/600x200)](https://www.youtube.com/@yPhil)
<p class="image-caption" style="text-align: center; margin-top: 0">"If you can find it anywhere, more power to you" (Snakefinger)</p>
