---
title: SuberCool 😎 0.0.1 is out
date: 2022-10-26 20:34:30
tags: python
---

I slapped [this little thing](https://framagit.org/yphil/subercool) together last week, it's ready now, here is the documentation.

Download subtitles in any language from the Gnome "Files" manager (Nautilus) or directly from the shell. 
![subercool-01](https://framagit.org/yphil/assets/-/raw/master/img/subercool/subercool-01.jpg)

## Installation
```shell sh
sudo apt install python3 exiftool
pip install requests zenipy 
mkdir -p ~/.local/bin
wget https://framagit.org/yphil/subercool/-/raw/master/subercool.py -O ~/.local/bin/subercool.py
```

### Nautilus plugin
Then to install it as a [Nautilus](https://apps.gnome.org/app/org.gnome.Nautilus/) plugin:
```shell sh
mkdir -p ~/.local/share/nautilus/scripts
ln -s ~/.local/bin/subercool.py ~/.local/share/nautilus/scripts/Download__Subtitles
mkdir -p ~/.config/nautilus
echo "<Control><Shift>s Download__Subtitles" >> ~/.config/nautilus/scripts-accels
```

![subercool-02](https://framagit.org/yphil/assets/-/raw/master/img/subercool/subercool-02.png)

## Configuration
Follow [the instructions](https://opensubtitles.stoplight.io/docs/opensubtitles-api/) to get an api key (it's quite simple really) which is a simple string of 32 characters.
Then, either pass this string as an argument like this:

```shell
subercool.py -k API_KEY ~/Videos/MyMovie.mp4
```

Or (simpler, and mandatory for the Nautilus plugin mode) put in in a file named `~/.local_variables.json` like this:

```json ~/.local_variables.json
{
    "opensubtitles_api_key": "API_KEY"
}

```

## Usage

```shell subercool.py -h
# subercool.py -h
usage: subercool.py [-h] [-k [API_KEY]] file

Download video subtitle(s)

positional arguments:
  file                  File to search in the opensubtitles database

options:
  -h, --help            show this help message and exit
  -k [API_KEY], --api_key [API_KEY]
                        API key to the opensubtitles database

See full documentation : https://framagit.org/yphil/subercool/
```
![subercool-03.png](https://framagit.org/yphil/assets/-/raw/master/img/subercool/subercool-03.png)

Yes, I have [syntax coloring in my shell](/blog/2022/10/18/Syntax-highlight-coloring-in-ZSH-shell/), get over it 🙂 

### Commit history
[Here](https://framagit.org/yphil/dotfiles/-/blob/master/.scripts/subercool.py).

Brought to you by yPhil ; Please (please?) consider [helping](https://liberapay.com/yPhil/donate) ❤️
