---
title: Import NetVibes dans Pétrolette 🇫🇷
date: 2021-02-21 19:10:02
tags:
- pétrolette
- JS
---
## Devlog

Dans ce journal, nous verrons comment Pétrolette est maintenant capable d'importer le fichier [OPML](https://en.wikipedia.org/wiki/Opml) d'export de [NetVibes](https://www.netvibes.com/).

### Rappel

[Pétrolette](http://petrolette.space/) est une page d'accueil de lecture d'actualités, [libre](https://linuxfr.org/users/philippemc/journaux/petrolette-1-1). Elle est immédiatement utilisable sans inscription avec la même URL dans le navigateur du bureau ou celui d'un appareil mobile.

## OPML

La structure du fichier est un peu étrange :

````xml
  <?xml version="1.0" encoding="UTF-8"?>
  <opml version="1.0">
  <head>
  <title>UserName</title>
  <type>Private</type>
  <creationDate>2021-02-05 06:56:35</creationDate>
  </head>
  <body>
  <outline title="Tab1" cols="4" layout="4-0">
  <outline title="FeedTitle" xmlUrl="https://www.example.com/rss" htmlUrl="https://www.example.com/rss" type="rss" row="562.5" col="1"/>
  <outline title="FeedTitle" xmlUrl="https://www.example.com/rss" htmlUrl="https://www.example.com/rss" type="rss" row="1562.5" col="1"/>
  </outline>
  <outline title="Blogs" cols="4" layout="4-0">
  <outline title="FeedTitle" xmlUrl="https://www.example.com/rss" htmlUrl="https://www.example.com/rss" type="rss" row="500" col="1"/>
  <outline title="15ISSD" xmlUrl="https://www.example.com/rss" htmlUrl="https://www.example.com/rss" type="rss" row="5000" col="1"/>
  <outline title="FeedTitle" xmlUrl="https://www.example.com/rss" htmlUrl="https://www.example.com/rss" type="rss" row="7000" col="1"/>
  </outline>
  <outline title="Tab2" cols="3" layout="3-0">
   <outline title="FeedTitle" xmlUrl="https://www.example.com/rss" htmlUrl="https://www.example.com/rss" type="rss" row="125" col="1"/>
   <outline title="FeedTitle" xmlUrl="https://www.example.com/rss" htmlUrl="https://www.example.com/rss" type="rss" row="250" col="1"/>
  </outline>
 </body>
</opml>
````

### Redondances

On peut se demander à quoi sert l'attribut "cols" de chaque tab, puisque les flux eux-mêmes ont un paramètre "col" pour indiquer où ils se trouvent dans le tab..?

Chez nous, c'est plus simple:

````javascript
[
  {"name":"news",
   "columns": [
     [
       {"url":"https://www.sciencemag.org/rss/weekly_news_email.xml","type":"photo","limit": 1,"status":"off"},
       {"url":"http://rss.sciam.com/ScientificAmerican-Global","type":"mixed","limit": 6,"status":"on"}
     ],
     [
       {"url":"https://www.reddit.com/r/worldnews.rss","type":"mixed","limit": 6,"status":"off"},
       {"url":"https://www.npr.org/rss/rss.php?id=1001","type":"mixed","limit": 4,"status":"on"}
     ],
     [
       {"url":"http://explainxkcd.com/rss.xml","type":"photo","limit": 4,"status":"on"},
       {"url":"http://xkcd.com/rss.xml","type":"photo","limit": 2,"status":"on"}
     ],
     [
       {"url":"https://www.ignant.com/category/architecture/feed/","type":"photo","limit": 6,"status":"on"}
     ]
   ]
  },
  {"name":"tek",
   "columns": [
     [
       {"url":"http://feeds.bbci.co.uk/news/technology/rss.xml","type":"mixed","limit": 6,"status":"on"},
       {"url":"http://feeds.feedburner.com/hackaday/LgoM/","type":"mixed","limit": 8,"status":"on"},

       {"url":"https://hackernoon.com/feed","type":"mixed","limit": 8,"status":"off"}
     ],
     [
       {"url":"https://hacks.mozilla.org/feed/","type":"mixed","limit": 4,"status":"on"},
       {"url":"http://feeds.arstechnica.com/arstechnica/index","type":"mixed","limit": 6,"status":"on"}
     ]
   ]
  }
]
````

### XMLtoJSON

Apres avoir [parsé le XML](https://framagit.org/yphil/petrolette/-/blob/master/public/js/PTL.util.js#L4), on se retrouve avec ça:

````javascript
[
  {
    "name": "Tab1",
    "cols": "2",
    "columns": [
      {
        "col": "1",
        "url": "http://example.com/rss"
      },
      {
        "col": "1",
        "url": "http://example.com/rss"
      },
      {
        "col": "2",
        "url": "http://example.com/rss"
      }
    ]
  },
  {
    "name": "Tab2",
    "cols": "3",
    "columns": [
      {
        "col": "1",
        "url": "http://example.com/rss"
      },
      {
        "col": "1",
        "url": "http://example.com/rss"
      },
      {
        "col": "3",
        "url": "http://example.com/rss"
      }
    ]
  }
]
````

### Objets et tableaux

Il nous faut donc simplifier tout ça, mais on ne peut pas itérer sur les feeds, puisqu'il ne sont pas rangés dans un container "tab"...

````javascript
    var xml2json = new PTL.util.XMLtoJSON(),
        objson = xml2json.fromStr(xml),
        dbparsed = JSON.stringify(objson),
        all = objson.opml.body.outline,
        allTabs = [];
    var totalNbOfFeed = 0;
    for (var nbOfTabs in all) {
      var tabs = all[nbOfTabs],
          feeds = tabs.outline;
      for (var key in tabs.outline[0]) {
        var tab = tabs[key],
            thisTab = {},
            thisTabFeeds = [],
            thisColFeeds = [];
        for (var nbOfFeeds in feeds) {
          var thisFeed = {};
          totalNbOfFeed++;
          thisFeed.status = "on";
          thisFeed.limit = 6;
          thisFeed.type = "mixed";
          thisFeed.url = feeds[nbOfFeeds]["@attributes"].xmlUrl;
          thisIndex = Number(feeds[nbOfFeeds]["@attributes"].col) - 1;
          if (!thisColFeeds[thisIndex]) thisColFeeds[thisIndex] = []; // C'est là que ça se passe
          thisColFeeds[thisIndex].push(thisFeed);
        }
        thisTabFeeds.push(thisColFeeds);
        thisTab.columns = thisColFeeds;
        thisTab.name = tab.title;
      }
      allTabs.push(thisTab);
    }
    PTL.util.console(PTL.tr('Found %1 groups containing %2 feeds', nbGroups, nbFeeds), 'success');
````

À la première occurrence du champ "col" on initialise un tableau qui va recevoir tous les flux (*feeds*) qui ont le même paramètre, qu'on met dans l'objet thisTab.columns, qu'on met dans le tableau allTabs.

On ajoute des valeurs par défaut à la volée: `status` (feed dé/plié), `limit` (le nombre d'articles), et `type` (texte, photo, mixed).

Donc au moment de l'import on teste

- Si le fichier est du json valide ;
- si ce fichier est un fichier prétrolette (on loope sur tous les tabs, qui doivent tous avoir un param. "columns") ;
- Ou si au contraire, c'est du XML (`xml.startsWith('<?xml version="1.0" encoding="UTF-8"?>')`)
- Sinon erreur

## Notification discrète

Pour avertir l'utilisateur autrement qu'avec un méchant dialog, j'ai bricolé un petit systeme de notification non intrusive:

````javascript
    $('#notify').fadeIn('fast', 'linear', function() {
      setTimeout(function() {
        $notify.fadeOut('slow');}, 5000);
    });
````

## Navigation au clavier

Ah oui, j'oubliais ; Pétrolette est depuis le début navigable au clavier, mais il fallait mettre le focus sur un onglet, c'est maintenant le cas au démarrage:

````javascript
$('#tabs').find('li[tabindex="0"]:first-child').focus();
````

## RERO

Voilà :) Prochaine épisode : Le [scrolling infini dans les flux](https://framagit.org/yphil/petrolette/-/issues/63).

Si vous voulez que Pétrolette importe un autre type de fichier de flux, merci de [l'attacher à un ticket](https://framagit.org/dashboard/issues).
