---
title: Jouer de la musique avec des jouets - RockBand Drums 🇫🇷
date:  2015-10-09
tags:
- audio
---

## Chers voisins et honorables membres du syndic

On trouve dans tous les bons placards des contrôleurs bizarres pour des jeux oubliés. Cannes à pèche, canons à Pokemon et autres lightguns à zombie, bien peu survivent au jeu avec lequel ils étaient vendus. Pourquoi ? La réponse, c'est le jeune Clément Michu (Région Parisienne) qui nous la donne lors d'un séjour à l’hôpital pour une légère électrocution : "J'ai péché aucun poisson, mes photos de Pokemon sont toutes floutes et pour les zombies je vais faire autrement" ; ces contrôleurs, souvent des machines très bien faites, ne servent qu'à jouer au jeu pour lequel ils ont été conçus — avec des exceptions bien sur: les maracas de "Samba de Amigo" (Sonic Team, 2000, Dreamcast) font bien tchicka tchicka même non branchés — ce journal va quelque part, je vous le jure.

## Back to front stage

Après Guitar Hero (RedOctane, 2005) qui propose au joueur de suivre des riffs de guitare en appuyant en rythme sur des boutons et c'est très rigolo, sont sortis d'autres "instruments" dont le micro (la console détecte juste le temps, pas la hauteur ni les paroles) la basse (comme la guitare mais moins de boutons) et un jour (ta-dtzimmmm) la **batterie** et là je laisse la parole au professeur Von Shredder, directeur d'études à l'IRCAM ; extrait: "Il ne vous aura pas échappé que dans ce cas précis le joueur reste au pas du paradigme de la _simulation_ pour demeurer dans celui de l'_exécution_, ce que lui confirmeront plusieurs parties, ou entre deux barres de rire le sujet va de fait acquérir les réflexes et la mémoire musculaire de base du batteur, au contraire du guitariste virtuel sus-mentionné, ou encore du dinosaure de Toy Story qui devant l'obstacle que constitue la descente du toit de la maison d'Andy, s'exclame "I played a lot of videogames, I wonder if the skills are transferaaaaaaaaaaa" ; fin de citation.

## Sk33l5

En pratique, il parait que c'est super dur ("Toutafé, en ce que" ça va merci professeur maintenant, vos élèves vous attendent). Que les modes "facile" et "medium" ne comprennent même pas l'usage de la pédale de kick. Après d'âpres lancers de baguettes ces merveilles finissent dans des placards, les unes après les autres. Tant de puissance rangée entre les chaussures de ski et les appareils à raclette, qu'on y songe.

!["Ce n'est pas mon modèle exact, pas pu retrouver la photo"](/images/Rock-Band-4-Wireless-Pro-Drum-Kit-for-Xbox-One.png)

Ils vendaient quand même ça dans les 150/200 balles, hein. Mais quand on regarde bien la chose (et surtout après, quand on a fini de faire ça et qu'on se met à joyeusement taper dessus) on réalise qu'on est en face d'une machine extrêmement bien conçue d'une part ; une armature en plastique ABS sur deux solides piliers d'alu, et très semblable, sinon supérieure en réalisation, à un équivalent IRL du genre Roland VDrums, avec des capteurs tout simplement sourcés sur le marché musical, d'au-tre-part ♫

Apparemment les premiers modèles (y'en a [plein](https://www.google.com/search?q=rockband+drums+controller&hs=X7W&channel=fs&source=lnms&tbm=isch&sa=X&ved=0CAgQ_AUoAmoVChMIhMiIw_CyyAIVSboUCh2U6AXJ&biw=1855&bih=1105)!) ne géraient pas la vélocité (CàD envoyaient la même info selon que tu tapasse comme une brute ou que tu effleurasse le bazar) mais celui-ci, si.

Note également qu'il n'y a, en standard, qu'une pédale, qu'on utilisera soit pour le kick (Poum) soit pour fermer le HH (Hi Hat, en français Charleston, Tzim). Pour faire les deux, il suffit de trouver une autre pédale et de la brancher, chaque canal MIDI ayant sa prise (jack 3.5 mono).

## Ouverture, standard, bonheur, pré

Ce truc est fabriqué par des gens qui ont l'habitude de faire des objets utilisables par des gens. Rien que le logement pour ranger les baguettes fait réfléchir à leur sens aigu de l'ergonomie ; c'est sobre, solide, no-design. Un projet tellement pérenne et sympathique que quelque part pendant la conception, un des ingénieurs (il se trouve que c'était une fille) s'est mise à s'agiter en disant eh les mecs, on leur vend ça 1300¥ et il vont juste jouer 20 minutes entre l'aspirateur et la TV pour mettre ça à la benne quand la console aura disparu ? No way, regarde ce super hardware qu'on a fait péter c'est le cas de le dire, on va pas quand même pas cantonner ce contrôleur à une niche de divertissement mort-née (ouais ils parlent comme ça dans les bureaux d'études quand le client est pas là) et donc on va lui coller un port MIDI OUT! (Un quoi ? A dit le commercial qui dormait "un plan media qui poutre" lui a susurré son stagiaire pour le rendormir) alors tout le monde "Rhooo" - Ben oui, comme ça quand les vrais zicos qui s'amusent à ces enfantillages en auront marre de se prendre des tôles face à leur petite sœur qui brame du Nicki Minaj dans le micro en plastique, ce qui doit tout de même être marrant 2 minutes faut admettre, eh ben ils enverront la gamine faire ses devoirs et ils brancheront ça dans un expandeur pour faire de la MUSIQUE! Puis elle s'est assise, et on n'a plus entendu que la trompette de "All the money or a simple life" des Dandy Warhols quelque part dans l'open-space.

Toute l'équipe craignait la réaction du directeur quand il découvrirait que ses propres ingénieurs avaient ouvert la machine a la compatibilité du _reste du monde_, truc pour lequel on raconte tout de même qu'il y a des tecos enterrés sous le parking d'Apple.

Eh ben le boss, il a vu le truc sur les protos qui revenaient de l'usine (c'est gros un port MIDI, ils ont repris la norme DIN, qui venait de perdre la guerre froide contre JACK) et a lu "MIDI OUT", il a remonté ses lunettes et là il a dit "je veux un MIDI IN".

On pourrait penser que c'est par pur souci de symétrie que Mr Hyong Tan a pris cette courageuse décision de capitaine d'industrie, mais ceux qui le connaissent vous diront que c'est parce qu'il avait compris que non content d'utiliser ce matériel, les musiciens voudraient développer le kit et y ajouter par exemple une snare (caisse claire) qui gère le rimshot (coup de baguette sur le cerclage) du genre de celle des VDrums de luxe. 开始狂欢吧!

## Branchages

Pas besoin d'ordinateur en principe, il suffit de brancher un câble MIDI entre le OUT du contrôleur (en pratique, un boîtier avec les contrôles de la console — qui marche avec 2 piles AA et pas d'alim, snif — et qui est lui-même clips-relié au kit par un port proprio) et le IN d'une machine compatible GM ([General Midi](https://en.wikipedia.org/wiki/General_MIDI)) d'allumer les deux et poum tchak. Expander MIDI, n'importe quel synthé sorti ces derniers siècles, machine à karaoke, machine à bulles ou/et à feu d'artifice (si) etc. En fait, c'est la meilleure solution, ces machines étant dédiées, elle répondent vite et pour [certaines](http://www.davesmithinstruments.com/product/tempest/), sonnent comme une plantigrade parturiente.

### Procédure

Pour mettre tout ça vite fait en place dans une distro récente:

```
apt install qtractor qjackctl calf-plugins fluid-soundfont-gm
```

- Lancer Qtractor (ou Ardour, c'est pareil) - en principe il se charge de lancer jackd
- Lancer QjackCTL et cliquer sur *Connections* et sur l'onglet *MIDI*
- Connecter la sortie (à gauche) de la carte son (ou carte MIDI dédiée, il en reste) à l'entrée *MIDI MASTER* de Qtractor
- Dans Qtractor, créer une piste MIDI
- Afficher le Mixer (F9) et clic-droit dans la zone grise au-dessus des contrôles de la piste, choisir "add plugin"
- Enlever le filtre sur le type de plugin à droite, pour les afficher tous (préférer la norme LV2, les plugins sont souvent portés dans toutes les normes et celle-ci est la plus récente) dans le champ de recherche taper fluid.. et PAF le voilà: [Calf FluidSynth](http://calf-studio-gear.org/#plugins)
- Cliquer sur "Soundfont" et charger /usr/share/sounds/sf2/FluidR3_GM.sf2
- Ouvrir le menu du 1er canal MIDI (celui qui dit "Grand Piano Yamaha") et descendre tooout en bas, les batteries sont là. Yen a plein. Brutes, room, studio, yen a 3 kits et 10 ou 12 variations, ça va aller ? Sinon ya plein d'[autres solutions](http://www.drumgizmo.org/wiki/doku.php?id=kits) mais on dépasse le cadre de ce journal
- Allumer le contrôleur du drumkit en appuyant sur start
- Sortir les baguettes du logement sous le tom de droite
- Baisser un chwya le volume :)
- 3, 4

### Tir aux soucis

En cas de silence innoportun (Test: On entend encore les voisins)

- Vérifier en cliquant-droit sur la piste qu'elle est bien en auto-monitor
- Que le controlleur du drumkit est ON (le logo XBox clignote) ya deux boutons qui comptent: START, et un autre planqué dessous (il y a un sticker pour l'indiquer) sans label particulier, appuie dessus
- Que ta carte audio/MIDI est en MIDI IN (elle l'est, change rien, il n'y a en principe rien à faire pour le MIDI, en tout cas de grâce, inutile de poster partout des histoires de "driver MIDI") mais surtout en audio OUT dans QJackctl
- Qu'elle est reliée à l'ampli et que le bon canal y est sélectionné
- Que Pulse Audio... Non, rien. J'ai rien dit.

N’hésite pas à appuyer sur record Ohhh tu vois tous ces carrés de couleur avec une bande en bas ? Ce sont les notes que tu as entrées et leur vélocité. Si tu osais, tu pourrais prendre ta souris et rattraper les pains que tu as commis en tentant de suivre Dave Lombardo. Mais nous les pros, on fait jamais ça, bien sur.

Sauver (Qtractor *veut* un dossier et te demande trois fois le nom directory/session/file, c'est comme ça, fais-le avant de comprendre). La prochaine fois, il suffira d'allumer les machines et de charger cette session.

## Coda

Une batterie électronique, c'est un rêve de gosse. Il suffit pour s'en convaincre, d'en laisser un cogner dessus en lui disant "vas-y tape, c'est fait justement pour ça" ses yeux lancent des étincelles.

Note que rien n’empêche d'utiliser des sons non percussifs, ou d'intégrer ça dans un kit pour déclencher d'autres sons, de vibraphone, de moteurs d'imprimantes, que sais-je ; ce truc est un instrument de musique. j'ai fait les drums de [Enemy](https://www.youtube.com/watch?v=EBWNcJ26JPc) et les bongos de [Lost Week](https://www.youtube.com/watch?v=O27yC-qLlMM) avec (ouais je sors [un nouvel album](http://manyrecords.com/?album=21) mais toi-même tu sais) et malgré quelques problèmes de latence qui sont uniquement le fait de mon ~~bouzin~~ ordinateur, ça a été un plaisir de chaque instant.

Demandez au syndic ;p
