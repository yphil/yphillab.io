---
title: Pétrolette 1.6 "Canicule"
date: 2022-09-24 21:03:39
tags:
- pétrolette
- JS
---

## What is it again?
[Pétrolette](https://petrolette.space) is a news reading home page, **Free, Libre and Open-Source**. It is immediately usable *without registration* with the same address (URL) in the browser of a computer, a telephone / tablet, or even a television.

## New, hot & dry
This new version focuses mainly on the server side and the management of a self-hosted instance, but many small improvements have also been made to ergonomics. Of course some small bugs have also been squashed, it's been so hot this summer 🙂

![petrolette.png](https://yphil.bitbucket.io/images/petrolette.png)

## New in this version
Full details are in [the changelog](https://framagit.org/yphil/petrolette/-/blob/master/CHANGELOG.md).

### New server launch methods

#### Launch Arguments

Pétrolette can now be started in a “local” HTTP version without redirection, in order to facilitate development; to do this, use the npm run dev command and visit http://localhost:8000.

#### Config file

When launching the server, if there is a file named petrolette.config.json at the root and containing this:

``` json
{
   "optionname": "value"
}
```
Said options are then (after validation, of course) taken into account; **this file is not versioned** to facilitate (git pull) updates. If really it turns out (or becomes, because of the number of options) necessary, an installation script will generate the ad-hoc file by asking questions to the user.

#### Relative paths

An old ([#50](https://framagit.org/yphil/petrolette/-/issues/50)!) user request that required more work / tests than expected, Pétrolette is now installable in a subdirectory.

#### Single-user version

This was a recurring request from pilots since the start of the project: “Why force me to synchronize my streams in the cloud if I am alone on my Pétrolette? I host myself precisely to avoid any external dependency” - note in passing that you can also host your own file server like DropBox but I too am tired of the very idea; in short, if the config file mentioned above contains this:
``` json
{
   "instanceType": "monoUser"
}
```
…then the feeds are written and read from a single `petrolette.feeds` file on the server. It is up to the host to protect this page, or maybe Pétrolette can / must take care of it, we'll see, let's discuss it in the issues.

#### HTTP redirect => HTTPS

I wanted to leave this upstream of the server (via solutions like iptables) but it's apparently impossible (a search for "https redirect with iptables" gets the answer that the browser uses port TCP/80 in http without TLS, which the server will not accept on its TCP/443 port for https), Pétrolette therefore redirects itself (in production mode) the HTTP port to the HTTPS port, the latter being easily configurable.

#### Publication date of articles

Display (in the help bubble / tooltip) of the publication / update date of the article if applicable, which is generally the case, contrary to

#### Image captions

In all these years of fiddling with RSS I had never come across a feed whose images had captions; these are therefore now managed.

#### Messages

The messages have been unified on the server *and* client side.

![Unified console messages](https://img.linuxfr.org/img/68747470733a2f2f692e706f7374696d672e63632f63436d7170706d382f616e696d2d636f6e736f6c652e676966/anim-console.gif "Client Messages")

### Bugs

#### Feed titles

The feed title was not updating immediately after change ; when clearing the name, the feed was called “New Feed” and did not recover its original name. Yes, ouch, and in addition this bug has been there for *years*.

#### (Keyboard) Focus

The focus wasn't obvious / intuitive enough ; and also not put on the tab at activation, it is now the case.
Translations

Some internal messages were not translated, they are now.

#### New host

The public instance https://petrolette.space has moved 🚀 from Ikoula (where our very small but not given server fell with each dispatch / version announcement) it moved to [@hydrosaas](https://mastodon.hydrosaas.com/@hydrosaas) ([offers](https://hydrosaas.com/vps/)) who offered us a BIG discount on the price to help the Pétrolette; we now have a more powerful server (CPU + RAM) and less expensive, thank you very much to the whole team, which is also very responsive, it is very nice to be able to join them on Mastodon <3

Of course, this migration ~~should be~~ is completely transparent :)

## In the garage (maintenance and development)

### Shares

This new single-user instance feature (for lack of a better term, because it's inaccurate, it's just that everyone sees - and edits - the same feeds) gives ideas to pilots, who envision a third type of use: a Pétrolette for an entire team.

A reflection is underway on the form it could take, for example with folders, mypetrolette.tld/arnaud, mypetrolette.tld/samuel, etc. or even a tab sharing system, or rather instance sharing between team members, where each member would manage their tabs.

While talking with a very active user about all this, I found myself dreaming of a Pétrolette that finally brings in some money, by selling support to teams that work with it… To be continued.

### Version v2.0.0 (:BREAKING)

The development of the future Pétrolette (code-named "Jetpack") in Scala continues, it just takes longer than expected (it's really hard!) thank you for participating to give me the energy to make a blog and share all that.

## No tickets, no problems

![yep](https://i.postimg.cc/nc4YJHBW/screenshot-2022-09-22-21-11-32-1052x1038.png)

It's in fact the opposite of a topic, but it's a pretty good feeling, [everything is ready](https://framagit.org/yphil/petrolette/-/issues/50) for v1.6.1, thank you for your attention, don't forget to tip your gas station attendant 🙂

Petrolette [needs you](https://liberapay.com/yPhil/).
