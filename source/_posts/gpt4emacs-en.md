---
title: gpt4emacs
date: 2023-06-23 02:13:05
tags:
- lisp
- AI
---

## Description

A modern [M-x doctor](https://en.wikipedia.org/wiki/ELIZA) for Emacs 😉

This module provides a simple prompt to query the [OpenAI](https://openai.com) API within [Emacs](https://www.gnu.org/software/emacs/), either on the region, the whole buffer, or out of the blue.

[![Not in real time 😉](/images/g4e.gif)](https://framagit.org/yphil/gpt4emacs)

It exposes only one single (asynchronous) command, uses the `configure` system to set the [API key](https://platform.openai.com/account/api-keys), the *temperature*, and the *model*, and pays special attention to human languages encodings and compatibility.

## Features
- Optional replacement of the selection
- Customizable `gpt-rules` directive to append to any further query, editable on-the-fly ;
- Optional display of the token data (Prompt, Completion, Total)
- UTF-8 aware

---

Brought to you by [yPhil](https://yphil.gitlab.io/blog). Please consider helping ♥ [here](https://liberapay.com/yPhil/donate) or [here](https://ko-fi.com/yphil). Icon by Buuf.
