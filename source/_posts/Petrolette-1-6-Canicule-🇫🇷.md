---
title: Pétrolette 1.6 "Canicule" 🇫🇷
date: 2022-09-23 20:20:10
tags: pétrolette
---
[Pétrolette](https://petrolette.space/) est une page d’accueil de lecture d’actualités, [libre](https://framagit.org/yphil/petrolette/-/blob/master/LICENSE) et [gratuite](https://liberapay.com/yPhil/). Elle est immédiatement utilisable **sans inscription** avec la même adresse (URL) dans le navigateur d’un ordinateur, d’un téléphone / tablette, ou encore d’une télévision.

Cette nouvelle version porte surtout sur le côté serveur et la gestion d’une instance auto-hébergée, mais beaucoup de petites améliorations ont été également apportées à l’ergonomie. Évidemment, quelques petits bugs ont également été écrasés, il a fait si chaud cet été 🙂

![petrolette.png](https://yphil.bitbucket.io/images/petrolette.png)

# Nouveautés de la version 1.6
Tous les détails sont dans [le journal des changements](https://framagit.org/yphil/petrolette/-/blob/master/CHANGELOG.md).

## Nouvelles méthodes de lancement du serveur
### Arguments de lancement
Pétrolette peut maintenant être démarrée en version « locale » HTTP sans redirection, afin de faciliter le développement ; pour ce faire, utiliser la commande `npm run dev` et visiter `http://localhost:8000`.

### Fichier de config
Au lancement du serveur, s’il se trouve à la racine un fichier nommé `petrolette.config.json` et contenant ceci:

```javascript
{
  "nomOption" : "valeur"
}
```

Lesdites options sont alors (après validation, bien sur) prises en compte ; ce fichier n’est *pas* versionné pour faciliter les (`git pull`) mises à jour. Si vraiment cela s’avère (ou devient, à cause du nombre d’options) nécessaire, un script d’installation générera le fichier ad-hoc en posant des questions à l’utilisateur.

## Chemins relatifs
Une vieille ([#50](https://framagit.org/yphil/petrolette/-/issues/50) !) demande d’utilisateur qui a demandé plus de boulot / tests que prévu, Pétrolette est maintenant installable dans un sous-répertoire.

## Version mono-utilisateur
C’était une demande récurrente des pilotes depuis le début du projet : « Pourquoi m’obliger à synchroniser mes flux dans le nuage si je suis seul sur ma Pétrolette ? Je m’auto-héberge _justement_ pour éviter toute dépendance externe » - note en passant qu’on peut également [héberger son propre serveur de fichier](https://remotestorage.io/) à la DropBox mais moi aussi l’idée même me fatigue ; bref, si le fichier de config évoqué plus haut contient ceci:

```javascript
{
  "instanceType" : "monoUser"
}
```



…alors les flux sont écrits et lus dans un fichier unique `petrolette.feeds` sur le serveur. À charge pour l’hébergeur de protéger cette page, ou peut-être que Pétrolette peut / doit s’en charger, il faut voir, on peut en discuter dans les tickets.

## Redirection HTTP => HTTPS
Je voulais laisser ça en amont du serveur (via des solutions comme `iptables`) mais c’est apparemment impossible (une recherche sur « https redirect with iptables » obtient comme réponse que le navigateur utilise le port TCP/80 en http sans TLS, ce que le serveur n’acceptera pas sur son port TCP/443 pour le https), Pétrolette redirige donc elle-même (en mode `production`) le port HTTP vers le port HTTPS, ces derniers étant bien configurables.

## Date de publication des articles
Affichage (dans la bulle d’aide / tooltip) de la date de publication / mise à jour de l’article si applicable, ce qui est généralement le cas, contrairement aux

## Légendes des images
Pendant toutes ces années à tripoter du RSS je n’étais jamais tombé sur un flux dont les images ont des légendes ; ces dernières sont donc maintenant gérées.

## Messages
Les messages ont été unifiés coté serveur *et* client.

![Messages de console harmonisés](https://i.postimg.cc/cCmqppm8/anim-console.gif)

# Bugs

## Titres de flux
Le titre du flux n’était pas mis à jour tout de suite après changement ; à l’effacement du nom, le flux s’appelait « Nouveau flux » et ne récupérait pas son nom d’origine. Oui, ouch, et en plus ce bug est là depuis des années.
## Accent
L’accent (focus) n’était pas assez visible ; et pas non plus mis sur l’onglet à son activation, c’est maintenant chose faite.
## Traductions
Certains messages internes n’étaient pas traduits.

# Dans le garage (maintenance et développement)

## Nouvel hébergeur 
L’instance publique https://petrolette.space a **déménagé** 🚀 : d’Ikoula (où notre tout petit mais pas donné serveur tombait à chaque dépêche / annonce de version) elle a bougé chez [@hydrosaas](https://mastodon.hydrosaas.com/@hydrosaas) (https://hydrosaas.com/vps/) qui nous a offert une GROSSE remise sur le prix pour aider la Pétrolette ; on a maintenant un serveur plus puissant (CPU + RAM) et moins cher, merci beaucoup à toute l’équipe, qui en plus est très réactive, il est bien sympathique de pouvoir les joindre sur Mastodon <3

Bien sur, cette migration ~~devrait être~~ est totalement transparente :)

## Partages
Cette nouvelle fonctionnalité d’instance mono-utilisateur (à défaut d’un meilleur terme, car c’est inexact, c’est juste que tout le monde voit - et édite - les même flux) donne des idées aux pilotes, qui envisionnent un troisième type d’usage : une Pétrolette pour toute une équipe. 

Une réflexion est en cours sur la forme que ça pourrait prendre, par exemple avec des dossiers, mypetrolette.tld/arnaud, mypetrolette.tld/samuel, etc. ou encore un système de partage d’onglets, ou plutôt de partage d’instance entre membres d’une équipe, où chaque membre gérerait ses onglets.

En discutant avec un utilisateur très actif de tout ça, je me suis pris à rêver d’une Pétrolette qui rapporte enfin un peu d’argent, en vendant du support à des équipes qui bossent avec… À suivre.

## Version v2.0.0 (:BREAKING)
Le développement de la future Pétrolette (code-nommée « Jetpack ») en Scala se poursuit, il prend juste plus de temps que prévu (c’est vachement dur !) merci de participer pour me donner l’énergie de faire un blog et partager tout ça.

# Plus de tickets, plus de problèmes
!["Clean slate"](https://i.postimg.cc/nc4YJHBW/screenshot-2022-09-22-21-11-32-1052x1038.png)

C’est en soi le contraire d’un sujet, mais ça fait bien plaisir, [tout est propre](https://framagit.org/yphil/petrolette/-/issues) pour la v1.6.1, merci de votre attention, n’oubliez pas le  pompiste 🙂

Pétrolette [a besoin de vous](https://liberapay.com/yPhil/).
