---
title: GPT makes me sound like a self-important student
date: 2023-08-07 20:00:02
tags:
- JS
- AI
---
I once read, quite a long time ago under a BSD dev's pen, "BSD is *designed*, but Linux is *grown*."

I'm quite happy to admit that my apps, basically all my apps, all the dev I've done, that I do and will do, are clearly grown.

It starts with an idea, and then it grows from there. Rhizomes, roots, offshoots, branches and sometimes flowers. Screens, states, then other screens, more states, then less screens and less states.

On Sunday, I tested my workout app, Dolores. And while it worked fine in my shady office, in the bright sunlight and the roar of the waves of the oceanfront, I could not:

1. Hear the beeps of the workout ;
2. Identify at a glance the stage I was in, whether it was a workout or rest.

> And I decided that this app needs a dedicated screen during the workout to display both the beeps and the workout stage. I began sketching out ideas for the new screen, envisioning a sleek and intuitive design.

> As I continued working on Dolores, I couldn't help but think about the concept of growth in software development. Each line of code I added was like a new branch on a tree, expanding the functionality and capabilities of the app. It was a constant cycle of brainstorming, implementing, and refining.

> In the following days, I delved into the codebase and started implementing the new screen. I carefully considered the user experience, making sure that the beeps were audible even in noisy environments and that the workout stage was clearly visible regardless of lighting conditions. Colors, font sizes and ergonomics were all taken into account to create a seamless and enjoyable user interface.

Yeah, GPT (well, [lex.page](https://lex.page/), ackchally) wrote those last 3 paragraphs (I wrote the rizhomes and branches thing - shut up, I like it - I wanted to kick him off, in retrospect maybe hence his Parisian second-grader tone, but no I started over and it was - believe it - even worse) I mean, I told him right away "can you be more pompous and full of yourself? I’m writing a blog post for RMS’s sake, jeez" and he persisted on this tune, look:

> After several iterations and user testing, the new screen was finally complete. I was proud of how it seamlessly integrated into the existing app, enhancing the overall user experience. It felt like a natural extension of the application, as if it had always been there.

O-My-GAWD Dude, you're embarassingly useless ; Lemme write my own bull, OK 🤓

With the new screen in place, Dolores is ready for another round of real-life beach front testing.

Yeah, this last one is me. What? Let me see *your* blog, mister "humble and witty" hm?
