---
title: Permuter les fenêtres i3 (ou autre WM Xorg) avec alt + tab 🇫🇷
date: 2022-10-18 12:41:17
tags: i3
---

## Alt + tab

Les gens sont divisés en deux groupes : Ceux qui changent de fenêtre avec le clavier, et les, hum, appelons les *les autres*.

Jusqu'à récemment, j'utilisais une [solution assez bricolo](https://github.com/altdesktop/i3ipc-python/blob/master/examples/focus-last.py) (qui fonctionnait, la plupart du temps) pour basculer entre les deux dernières fenêtres utilisées. Et puis la lumière est entrée dans la pièce sous la forme d'[une seule commande](https://github.com/sagb/alttab). Disponible dans les dépôts principaux 😲 

``` shell
apt install alttab
```

J'ai passé tellement de temps à ignorer l'existence même de ce joyau parce qu'il est si difficile à rechercher, je ne me souviens même pas par quel genre de miracle je suis tombé dessus. Bon, c'est là maintenant.

![Alt-tab dans i3 (Xorg)](/images/yPhil-alttab.gif)

En fait, [toute ma config / workflow](https://framagit.org/yphil/dotfiles) a changé depuis.

## En i3

``` shell ~/.config/i3/config
exec_always --no-startup-id alttab -d 1 -t 132x132 -i 128x128 -bg "#262626" -fg "#262626" -frame "#15539e"
```

Je n'ai même pas eu à faire de [liaison (binding)](/blog/2022/10/17/Lister-les-raccourcis-clavier-d-i3/) ce truc *prend* Alt + tab (à moins d'être [configuré différemment](https://github.com/sagb/alttab/blob/master/README.md) bien sur) et-c'est-tout 🙂
