---
title: Coloration syntaxique dans le shell (ZSH) 🇫🇷
date: 2022-10-18 13:24:31
tags: 
- shell
---

Voici une autre chose qui devrait être installée par défaut, et qui en fait l'est [pour moi](https://framagit.org/yphil/dotfiles), depuis environ cinq bonnes années :

## Coloration de la syntaxe dans le shell

Et on ne parle pas de la couleur de l'invite (*prompt*) ni même des sorties de commande, je veux dire, ces choses sont généralement prises en charge, voici la sortie de `ls -la` dans un Ubuntu vanilla :

![Extrait de sortie Linux ls -la (avec utilisateur et groupe tronqués)](/images/zsh-ls.png)

Non, il est question de ***ce que tu tapes en vrai dans ton shell en temps réel*** :

![Mise en évidence de la syntaxe / coloration dans le shell ZSH](/images/zsh-syntax-coloring.png)

Dans cet exemple (`tmux` dans `gnome-terminal`) on a une assez belle demo : Pour commencer, les commandes sont validées en temps réel : si tu entres un nom de commande qui n'est pas dans le chemin il est coloré en rouge, les commandes valides sont en vert, **toutes** les commandes et aliases.

Mais le plus puissant est la coloration des commandes de script, bien sur. Rappel : Les language de script de Bash et de Zsh sont différents mais compatibles, en fait personnellement bien qu'utilisateur de Zsh depuis bien 15 ans maintenant, tous mes scripts shell commencent par `#!/usr/bin/env bash` et typiquement, la boucle `for` ci-dessus est en Bash (en Zsh c'est plus court, pas besoin du `do` / `done`) 🤓 

## Pour installer ce truc indispensable et n'y plus jamais penser

``` shell sh
cd ~/src
git clone https://github.com/zsh-users/zsh-syntax-highlighting
```

### dans `.zshrc`

``` shell ~/.zshrc
[[ -a ~/src/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]] && . ~/src/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
```

On teste la présence du fichier et on le source s'il est trouvé ; Le `.` signifie `source`. Mais *toi-même tu sais* ça.
