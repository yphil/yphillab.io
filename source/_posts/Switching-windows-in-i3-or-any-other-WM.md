---
title: Switching windows in i3 (or any other Xorg WM) with alt + tab
date: 2022-10-18 11:47:13
tags: i3
---

## Alt + tab

People are divided in two groups : Those who use switch windows with the keyboard, and the, hum, let's call them *the others*.

Until about recently, I was using a [quite hacky solution](https://github.com/altdesktop/i3ipc-python/blob/master/examples/focus-last.py) (that worked, mostly) to  just toggle between the two last used windows. And then the light entered the room in the form of [a single command](https://github.com/sagb/alttab). Available in the main repos 😲 

``` shell
apt install alttab
```

I spent so much time ignoring the very existence of this gem because it's so hard to search for, I don't even remember by what kind of earthy miracle I stumbled upon it. Well, it's there now. 

![Alt-tab in i3 (Xorg)](/images/yPhil-alttab.gif)

In fact, [my whole config / workflow](https://framagit.org/yphil/dotfiles) has changed since.

## In i3

``` shell ~/.config/i3/config
exec_always --no-startup-id alttab -d 1 -t 132x132 -i 128x128 -bg "#262626" -fg "#262626" -frame "#15539e"
```

I didn't even have to do any [key binding](/blog/2022/10/17/List-i3-keyboard-shortcuts/), this things (unless [told differently](https://github.com/sagb/alttab/blob/master/README.md) of course) steals Alt+tab and-that's-it 🙂
