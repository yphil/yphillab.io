---
title: gpt4emacs 🇫🇷
date: 2023-06-23 02:13:05
tags:
- lisp
- AI
---

## Description

On a de l'IA depuis longtemps dans [Emacs](https://www.gnu.org/software/emacs/) avec `M-x doctor` ; c'était [basique](https://en.wikipedia.org/wiki/ELIZA), mais déjà assez fascinant ; ma fille a appris l'anglais en jouant à l'interroger sur les choses de la vie.

[![Pas en temps réel 😉](/images/g4e.gif)](https://framagit.org/yphil/gpt4emacs)

[gpt4emacs](https://framagit.org/yphil/gpt4emacs) est un petit module (< 250l) qui permet d'interroger l'API [OpenAI](https://openai.com) directement depuis le minibuffer d'Emacs à partir d'une région, de l'ensemble du buffer ou de rien du tout, juste une question.

Il expose une unique commande (asynchrone), utilise `customize` pour définir la [clé API](https://platform.openai.com/account/api-keys), la température et le modèle, et porte une attention particulière à la compatibilité avec un maximum de langages humains.

### Fonctionnalités
- Remplacement optionnel de la région ;
- Affichage optionnel des données de jeton (Prompt, Completion, Total)
- Directive `gpt-rules` personnalisable pour ajouter à toute autre requête, modifiable en temps réel ;
- Compatible UTF-8.

---

Merci de penser à aider ♥ [ici](https://liberapay.com/yPhil/donate) ou [là](https://ko-fi.com/yphil). Icône par Buuf.
