---
title: Lister les raccourcis clavier d'i3 🇫🇷
date: 2022-10-17 15:39:33
tags: 
- i3
- python
---

## Ça va devenir vraiment tekos 👓 par ici 

Hier après-midi j'ai eu un moment de libre et j'ai décidé de le perdre en grattant une *minuscule* démangeaison ; J'ai donc écrit un script pour analyser le fichier de configuration i3, extraire les informations sur les liaisons `keysym`  (oui, y compris les modes) et les présenter de manière utile et opportune.

![Raccourcis clavier i3](/images/i3Shortcuts.jpg)

Ça pourrait être mieux ; ce que j'aimerais ressemblerait à [which-key](https://elpa.gnu.org/packages/which-key.html), qui affiche les combinaisons de touches disponibles tout au long de la frappe, c'est-à-dire lorsque tu tapes `$mod + a` pour lancer une application, il te présente les combinaisons de touches déjà configurées ; Mais là c'est juste une liste, présentée de la façon la plus ergonomique possible.

Donc maintenant chez moi d'une part `$mod+Shift+k` mais également `k` dans n'importe quel mode affiche la fenètre en question.

Techniquement, il s'agit d'un script Python3 qui lit et analyse `~/.config/i3/config` (je viens de réaliser que je n'ai rien écrit en cas d'absence de ce fichier, d'autant qu'il peut aussi se trouver à `~/.i3config`, je vais reprendre ça demain, pour le moment c'est du *quick-and-dirty*, c'est un fichier adapté à mon environnement (spécifiquement [mon fichier de config](https://framagit.org/yphil/dotfiles/-/blob/master/config/i3/config#L158)):

``` shell ~/.config/i3/config
# MODE:
bindsym $mod+a mode "$app_launch_mode"
mode "$app_launch_mode" {
    bindsym a exec ardour, mode "default"
	# (...)
}

# MODE:
bindsym $mod+b mode "$dummy_launch_mode"
mode "$app_launch_mode" {
    bindsym a exec somethingelse, mode "default"
	# (...)
}
``` 

Donc YMMV ; et qui, selon les arguments passés en ligne de commande, les affiche dans la console (`stdout`) ou sous la forme d'une page formatée en [Pango](https://docs.gtk.org/Pango/pango_markup.html) qui peut ensuite être lue par quelque chose de standard comme un navigateur ou [zenity](https://help.gnome.org/users/zenity/stable/).

Si ce script peut être universel il le sera, mais réfléchis bien au problème : Il faudrait littéralement (re)implémenter le parser d'i3 pour non seulement trouver les modes, mais également lire les variables si comme moi tu caches des fonctions complèxes - en l'occurence le nom et les commandes d'appel des modes - derriere des variables avec des nom normaux lisibles par des humains ; More later.

``` shell
# px-i3-keyboard-shortcuts.py -h
usage: px-i3-keyboard-shortcuts.py [-h] [-t FORMATTING] [-o OUTTYPE] [-f OUTFILE]

options:
  -h, --help            show this help message and exit
  -t FORMATTING, --formatting FORMATTING
                        Format of the output: html (default) / text
  -o OUTTYPE, --outtype OUTTYPE
                        Output type: stdout (default) / file / full (both)
  -f OUTFILE, --outfile OUTFILE
                        Temp. out file PATH (defaults to /tmp/.i3-kb-bindings.html)

```

Normalement, je passe juste la page au navigateur par défaut, mais `xdg-open` est actuellement cassé dans Firefox snap 😕

Source [ici](https://framagit.org/yphil/dotfiles/-/blob/master/.scripts/px-i3-keyboard-shortcuts.py).

## En i3

``` c++ ~/.config/i3/config
set $i3_keyboard_shortcuts "~/.scripts/px-i3-keyboard-shortcuts.py -t html -o file && zenity --text-info --title='i3 Keyboard Shortcuts' --width=640 --height=800 --html --window-icon=info --filename=/tmp/.i3-kb-bindings.html"

bindsym $mod+Shift+k exec $i3_keyboard_shortcuts
```

Mais aussi dans tous les modes (c'est-à-dire, `k` lorsque tu es dans un mode et que tu as oublié les touches)

``` c++ ~/.config/i3/config
bindsym $mod+z mode "$system_mode"
mode "$system_mode" {
    bindsym et exec --no-startup-id $exit_script déconnexion, mode "par défaut"
    bindsym h exec --no-startup-id ~/.scripts/px-hdmi-reset.sh 2, mode "par défaut"
    bindsym Shift+h exec --no-startup-id $exit_script hibernate, mode "default"
    bindsym k exec --no-startup-id $i3_keyboard_shortcuts # <= ICI
    bindsym l exec --no-startup-id $exit_script lock, mode "default"
    bindsym r redémarrage, mode "par défaut"
    bindsym Shift+r exec --no-startup-id $exit_script reboot, mode "default"
    bindsym s exec --no-startup-id $exit_script suspend, mode "default"
    bindsym Shift+s exec --no-startup-id $exit_script shutdown, mode "default"

    bindsym Mode de retour "par défaut"
    bindsym Mode d'échappement "par défaut"
}
```
C'est tout merci bonne semaine 🤖 
