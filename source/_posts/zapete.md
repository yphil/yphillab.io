---
title: Zapete - A remote control for your PC
date: 2024-02-12 02:16:28
tags: 
- JS
---

## Zapete: A remote control for your PC

A remote control for any computer in your local network.

[![Zapete](https://mobilohm.gitlab.io/img/shots/zapete.png)](https://gitlab.com/yphil/zapete)

I used to use something like this on my Palm Pilot in the 90s to control my TV & VCR with IRda. Nowadays normal people use a PC connected to a non-smart TV, so I needed that again ; It works better than a BT keyboard.

## Installation

``` ejs
git clone https://gitlab.com/yphil/zapete.git
cd zapete
npm install
```

## Usage

Run in on the target machine

```js
npm start
```

Scan the QRcode or point any browser to the displayed address, and tap "connect". Create buttons, send commands, everything is peachy.
