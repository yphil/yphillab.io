---
title: Titre de la fenêtre courante dans i3status 🇫🇷
date: 2022-10-13 12:37:21
thumbnail: i3-window-title-in-status.png
tags:
- i3
- python
---

<style>

.imagediv {
    position:relative;
    display: inline-block;
}

@keyframes animate {
    0% {
        opacity: 0;
    }

    50% {
        opacity: 0.7;
    }

    100% {
        opacity: 0;
    }
}

.imagediv::after {
    animation: animate 5s ease infinite;
    content:"↘️";
    position:absolute;
    left:150px;
    bottom:15px;
}

</style>

Étant donné la nature même d'i3, le nom de la fenêtre actuelle peut être difficile, voire impossible à déchiffrer, en particulier ces looongs titres de pages web.

[Le script Python suivant](https://framagit.org/yphil/dotfiles/-/blob/master/.scripts/px-i3-window-name.py) obtient ledit titre - ne s'étouffe pas lorsqu'il n'y en a pas - et l'envoie dans un fichier d'une ligne qui peut être lu à son tour par i3status.

<div class="imagediv">
    <img src="/blog/images/i3status.png">
</div>


Voir `~/.config/i3/i3status.conf` plus bas.

``` python
#!/usr/bin/env python3

import os
import i3ipc
from html import escape

TMPFILE = os.path.expanduser("/tmp/.current_window_name.txt")

i3 = i3ipc.Connection()

thisId = 0

root = i3.get_tree()

def writeTmpFile(wname):
    if wname:
        wname = escape(wname)
    else:
        wname = 'Unnamed window'

    with open(TMPFILE, 'w') as the_file:
        the_file.write(wname)

def on_window(i3, e):
    global thisId
    if e.change == "focus":
        thisId = e.container.id
        writeTmpFile(e.container.name)
    if e.change == "title":
        if e.container.id == thisId:
            writeTmpFile(e.container.name)

def on_workspace(i3, e):
    if e.change == "focus":
        writeTmpFile("Workspace " + e.current.name)

i3.on('window', on_window)
i3.on('workspace', on_workspace)

i3.main()
```
## in `~/.config/i3/i3status.conf`

``` c++ ~/.config/i3/i3status.conf
order += "read_file current_window_name"

read_file current_window_name {
    format = "<span foreground='#ff8c00'>%content</span>"
    format_bad = ""
    path = "/tmp/.current_window_name.txt"
}
```

## Ne pas oublier d'installer i3ipc

``` shell
python3 -m pip install i3ipc
```

NB si vous faites les choses [comme il faut](https://framagit.org/yphil/dotfiles), inutile de retenir tout ça ; *Tout* ce qui précède est **automatiquement** installé sur **chacune** de mes nouvelles machines ; ça prend quelques minutes en fonction de la météo marine.

Restez à l'écoute pour en savoir plus à ce sujet - déploiement et maintenance automatiques à l'aide d'un seul script shell simple en place de mastodontes comme [Guix](https://www.youtube.com/watch?v=iBaqOK75cho) - entre autres.
