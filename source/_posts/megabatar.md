---
title: MegaBatar - Joining the cohort
date: 2024-06-03 05:16:28
tags: 
- gamedev
- python
---

## Blasting Off to the Game Dev Elite: Crafting "MegaBatar" with Godot and GitLab CI/CD

I've always wanted to be part of the game dev 'leet. The journey to releasing my first (not really, but yeah) game, [MegaBatar](https://yphil.itch.io/megabatar), has been a wild ride ; Godot made the entire process not only feasible, but also quite enjoyable.

[![MegaBatar](/images/megabatar_00.png "MegaBatar")](https://yphil.itch.io/megabatar)

### Enter MegaBatar: A Retro Space Shooter with a Modern Twist

Meet MegaBatar, my take on the classic space shooter, but with all the flashy modern effects you’d expect in 2024. Think pulsating lasers, explosive particle effects, and a soundtrack that makes your adrenaline pump. It's like stepping into an arcade from the future.

[![MegaBatar on itch.io!](/images/megabatar_01.png "MegaBatar on itch.io!")](https://yphil.itch.io/megabatar)

## The Godot Grind: Making Everything Yourself

Godot is amazing, but it's no plug-and-play engine. It's more like a DIY spaceship kit: you have to bolt on the wings, weld the engine, and fine-tune the laser cannons yourself. It’s perfect for control freaks who love to get their hands dirty.

Godot forces you to be resourceful and creative, a bit like those early '90s Amiga developer one-man-and-a-half teams who squeezed every drop of performance out of their hardware.

## GitLab CI/CD: Remote Build and Deploy

Now, what's the use of building a killer game if you have to upload **one frigging gigabyte** of binaries at each iteration? Setting up a CI/CD pipeline meant I could focus on working on my game while GitLab handled the boring stuff.

Here's a peek at my GitLab CI/CD config:

```yaml
image: barichello/godot-ci:4.2.2

stages:
  - export
  - deploy

variables:
  EXPORT_NAME: megabatar
  ITCHIO_NAME: yphil/megabatar

linux:
  stage: export
  script:
    - mkdir -v -p build/linux
    - godot --headless --verbose --export-release "Linux/X11" ./build/linux/$EXPORT_NAME.zip
  artifacts:
    name: $EXPORT_NAME-$CI_JOB_NAME
    paths:
      - build/linux
  only:
    - master

windows:
  stage: export
  script:
    - mkdir -v -p build/windows
    - godot --headless --verbose --export-release "Windows Desktop" ./build/windows/$EXPORT_NAME.exe
  artifacts:
    name: $EXPORT_NAME-$CI_JOB_NAME
    paths:
      - build/windows
  only:
    - master

mac:
  stage: export
  script:
    - mkdir -v -p build/macosx
    - godot --headless --verbose --export-release "Mac OSX" ./build/macosx/$EXPORT_NAME.zip
  artifacts:
    name: $EXPORT_NAME-$CI_JOB_NAME
    paths:
      - build/macosx
  only:
    - master

itchio:
  stage: deploy
  script:
    - butler push ./build/linux $ITCHIO_NAME:linux-beta
    - butler push ./build/windows $ITCHIO_NAME:windows-beta
    - butler push ./build/macosx $ITCHIO_NAME:mac-beta
  dependencies:
    - linux
    - windows
    - mac
  only:
    - master
```

With this setup, every push to the master branch kicks off a build for Linux, Windows, and macOS, then [deploys the builds to itch.io](https://yphil.itch.io/megabatar). Yeah 😎

The Tool Chest

- Of course: [Godot 4.2.1](https://godotengine.org/)
- Text editor: [Emacs on Eglot/LSP](https://hristos.lol/blog/godot-engine-editing-gdscript-with-emacs/)
- 3D modeling & texturing: [Blender 4.1](https://www.blender.org/)
- Vector graphics: [Inkscape 1.1](https://inkscape.org/)
- Bitmap graphics: [GIMP 2.10.30](https://www.gimp.org/)
- Music: [Sunvox 2.1.1c](https://warmplace.ru/soft/sunvox/manual.php)

## The Future is Now

The feedback on MegaBatar has been less than stellar, and the floor's the limit. Follow me for the ride ;)


## Source

Is if FLOSS? I don't know, I really don't give a duck anymore ; Source is [here](https://gitlab.com/potatorock/megabatar), access on huh, demand.

### Random code examples

Switching from 2D to 3D by dollying *and* rotating the camera

```gdscript
func switch_cams(from, to):
    var from_gt = from.global_transform
    var from_fov = from.fov

    var cam_tween = create_tween()
    cam_tween.tween_property(from, "global_transform", to.global_transform, 3.0)
    cam_tween.parallel().tween_property(from, "fov", to.fov, 3.0)
    cam_tween.tween_callback(reset_cam.bind(from, to, from_gt, from_fov))
    
func reset_cam(from, to, gt, fov):
    from.current = false
    to.current = true
    from.global_transform = gt
    from.fov = fov
    switched_cams.emit()
```

Start the looped part of the song (almost right) after the intro:

```gdscript

func _play_music_intro():
    $MusicIntro.play()
    if !$MusicIntro.finished.is_connected($MusicLoop.play):
        $MusicIntro.finished.connect($MusicLoop.play)
```

![MegaBatar cover_box_art](/images/megabatar-cover_box_art.png "cover_box_art")


Want to know more? Just ask. Nicely.
