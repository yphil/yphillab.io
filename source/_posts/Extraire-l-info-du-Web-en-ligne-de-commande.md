---
title: Extraire l'info du Web en ligne de commande 🇫🇷
date: 2022-10-14 11:41:16
tags:
- shell
- i3
---

<style>

.imagediv {
    position:relative;
    display: inline-block;
}

@keyframes animate {
    0% {
        opacity: 0;
    }

    50% {
        opacity: 0.7;
    }

    100% {
        opacity: 0;
    }
}

.imagediv::after {
    animation: animate 5s ease infinite;
    content:"↘️";
    position:absolute;
    right:110px;
    bottom:15px;
}

</style>

Avant même de [coder un lecteur RSS](/blog/tags/petrolette) et de [l'utiliser toute la journée](https://petrolette.space/), je cherchais un moyen efficace de connaître l'heure et le mouvement de la marée, et un flux RSS semblait une bonne idée.

<div class="imagediv" title="test tooltip">
    <img src="/blog/images/i3status.png">
</div>

Il me semble même (pas vraiment sûr) me souvenir d'une telle ressource, mais si elle a jamais existé, elle est tombée maintenant de toute façon ; et pour cause : il n'est pas une mince affaire de servir potentiellement des centilliards de flux RSS, un par plage / port / côte.
Donc j'utilisais des sites Web. Trouver le navigateur, se tromper d'URL, ce genre de choses, mais je veux dire on est des programmeurs, on ne fait rien trois fois avant d'écrire un script pour le faire à notre place.

Le truc est que le "grattage" (*scraping*) Web est généralement le cheval de bataille des grands frameworks, j'en sais quelque chose, étant l'auteur de [Feedrat](https://www.npmjs.com/package/feedrat) et [Favrat](https://www.npmjs.com/package/favrat), qui chassent, trouvent et signalent respectivement les flux RSS et les favicons dans les pages Web, mais avec Nodejs et une montagne de dépendances.

Récemment, j'ai remarqué que j'utilisais le même site depuis des mois, et qu'il était vraiment stable. J'ai donc décidé de rechercher "web scraping from the shell" et j'ai trouvé [hxnormalize](https://www.w3.org/Tools/HTML-XML-utils/man1/hxnormalize.html) et [hxselect](https://www.w3.org/Tools/HTML-XML-utils/man1/hxselect.html) ; Ces deux commandes sont disponibles dans le paquet `html-xml-utils`.

Avec ces deux commandes, nous pouvons

- *Lire* du HTML (concrètement, disposer d'un objet représentant le [DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model) de la page, et non plus d'une simple chaine de caractères.)
- Sélectionner n'importe quel élément dans ce DOM

Nous n'avons donc besoin que d'un véhicule, d'un *vaisseau* pour nous transmettre la page Web, et avec ces deux commandes, faire quelque chose d'intéressant / valable / utile. Ce vaisseau est `wget`. Ou `curl`. Allez, `wget`, c'est celui que je connais le mieux.

``` shell Installer les packages nécessaires pour ce tutoriel (Debian)
apt install html-xml-utils lynx wget
```
Oui, on va avoir besoin de `lynx` plus tard, mais de toute façon tu devrais avoir ces commandes installées sur toutes tes machines [tout le temps](https://framagit.org/yphil/dotfiles), allons donc.

Voici [le site Web que je consulte](https://www.cabaigne.net/france/bretagne/saint-quay-portrieux/horaire-marees.html) pour obtenir les prochaines heures de marée de n'importe quel endroit de la terre :

![Ou juste F12](/images/SaintQuay.png)


Mais nous, on veut ces données disponibles directement sur le bureau, comment faire ça juste avec un bète script shell ? Clic droit sur l'élément, sélectionner "Inspecter" ; En visant bien, on obtiend le résultat de l'image ci-dessus.

Note le nom (en fait le *type* et la *classe*) de l'élément, dans notre cas `p.lead` et nous voilà en chemin 😎

## Tous au clavier

Testons ça : entre la commande suivante dans la console - `zsh` et `bash` fonctionnent tous les deux - tu as installé `wget`, hm ?

``` shell
echo "https://www.cabaigne.net/france/bretagne/saint-quay-portrieux/horaire-marees.html" | wget -O- -i- | hxnormalize -x | hxselect -i "p.lead"
```

Et voici la sortie

``` shell
--2022-10-13 20:03:06--  https://www.cabaigne.net/france/bretagne/saint-quay-portrieux/horaire-marees.html
Resolving www.cabaigne.net (www.cabaigne.net)... 000.00.00.000, 000.00.0.000, 000.00.0.000, ...
Connecting to www.cabaigne.net (www.cabaigne.net)|172.67.71.187|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [text/html]
Saving to: ‘STDOUT’

-                               [     <=>                                    ]  48.02K  21.7KB/s    in 2.2s

2022-10-13 20:03:09 (21.7 KB/s) - written to stdout [49170]

FINISHED --2022-10-13 20:03:09--
Total wall clock time: 4.0s
Downloaded: 1 files, 48K in 2.2s (21.7 KB/s)
<p class="lead"><img src="/site/images/temps.png"></img> <span class="label label-danger">marée
                      haute</span> Le jeudi 13 octobre 2022 à <b>22:10</b></p>%
```

Bingo ! Dans cette petite chaîne se trouvent les 2 infos dont nous avons besoin :

- L'état actuel de la marée (dans notre cas "haute" (high))
- La date/heure de la prochaine marée

Il ne reste plus qu'à se débarrasser du formatage HTML, ce que seul un débutant entreprendrait : l'analyse HTML (ou n'importe quel langage de balisage d'ailleurs) est une douleur dont la mémoire ne disparait jamais vraiment, demande à n'importe quelle pauvre âme ayant déjà essayé.

Non, ce que nous allons faire est de juste demander poliment à Lynx (`lynx` *est* installé) dont le travail est de faire *pile cela* (analyser du HTML) de nous sortir une chaîne propre et non formatée :

``` shell
echo "https://www.cabaigne.net/france/bretagne/saint-quay-portrieux/horaire-marees.html" | wget -O- -i- | hxnormalize -x | hxselect -i "p.lead" | lynx -stdin -dump
```

Sortie

``` shell
FINISHED --2022-10-13 20:10:15--
Total wall clock time: 0.7s
Downloaded: 1 files, 48K in 0.2s (219 KB/s)
    [temps.png]
   marée haute Le jeudi 13 octobre 2022 à 22:10
```
Oh, flute, il y a une balise sur notre chemin. Regarde à nouveau l'image ci-dessus. Saperlipopette, ce $*=)! de

``` html
<img src="/site/images/temps.png">
```

Est physiquement *dans* le `div` même, pas moyen d'éviter ça. On va juste l'extraire de la chaîne reçue en utilisant ce bon vieux `sed`

``` shell
echo "https://www.cabaigne.net/france/bretagne/saint-quay-portrieux/horaire-marees.html" | wget -O- -i- | hxnormalize -x | hxselect -i "p.lead" | lynx -stdin -dump | sed '1d'
```

Sortie

``` shell
FINISHED --2022-10-13 20:20:26--
Total wall clock time: 1.7s
Downloaded: 1 files, 48K in 0.2s (194 KB/s)
   marée haute Le jeudi 13 octobre 2022 à 22:10
```
## Tous à la plage 😎

Voici le script que j'ai écrit pour obtenir l'heure et le mouvement de la prochaine marée directement dans ma barre [i3status](/blog/tags/i3), accessible en un clin d'œil ; Passer simplement l'url en premier argument.

``` shell ~/.scripts/px-i3-next-tide-time.sh [url]
#!/usr/bin/env bash

PHRASE=$(echo "$1" | wget -O- -i- | hxnormalize -x | hxselect -i "p.lead"  | lynx -stdin -dump | sed '1d')

MOVE=$(echo $PHRASE | awk '{print $2}')
TIME=$(echo $PHRASE | awk '{print $9}')

[[ $MOVE = "basse" ]] && ICON="" || ICON=""

echo "$TIME $ICON" > /tmp/.next_tide_time.txt
```

## Dans i3

``` c++ ~/.config/i3/config
set $next_tide_script ~/.scripts/px-i3-next-tide-time.sh [url]
exec_always --no-startup-id $next_tide_script

```
À ce stade, le script s'exécutera à chaque démarrage ou redémarrage d'i3 ; Si - comme moi - tu veux qu'il s'exécute toutes les heures quoi qu'il arrive, utilise ça (avec ton propre `${MYSCRIPTDIR}` bien sur)

``` shell
(crontab -l 2>/dev/null; echo "0 * * * * ${MYSCRIPTDIR}/px-i3-next-tide-time.sh") | crontab -
```
Oui, c'est une entrée crontab directement depuis le shell, sans avoir à utiliser `visudo` ou autre non-sens, oui je sais que tu l'as lu ici en premier, [merci beaucoup](https://liberapay.com/yPhil/).

``` c++ ~/.config/i3/i3status.conf
order += "read_file next_tide_time"

read_file next_tide_time {
    format = "<span foreground='#ffffff'></span> <span foreground='#3daee9'> %content</span>"
    format_bad = ""
    path = "/tmp/.next_tide_time.txt"
}

```

C'est tout, à la prochaine fois, restons réels. Oh, et attention aux oursins 🐚
