---
title: From Web to Mobile with Capacitor
date: 2023-07-16 12:10:27
tags:
- mobile
- JVM
- JS
---

## I'm a Mobile Developer! (Nobody tells me anything)

- Code: https://framagit.org/yphil/dolores

The mobile development landscape has undergone numerous changes over the years. In the past, mobile developers were restricted to using specific languages: Java for Android, Objective-C and Swift for iOS 😱 But now, a revolution is underway; "full-stack" web developers can create complete mobile applications using their **decades** of experience in HTML, JavaScript, and CSS.

![Android](https://developer.android.com/static/images/brand/Android_Robot.png "That's the official logo")

## TL;DR: Where's the Stuff?
In this post, we'll explore how web developers can venture into the mobile world using [Capacitor](https://capacitorjs.com/), an open-source framework that bridges the gap between your code and your phone. It allows you to build full-fledged mobile apps using HTML, JavaScript, and CSS, without having to deal directly with Java — you *know* what I mean - I made [Dolores](https://framagit.org/yphil/dolores) because I needed it, but it also serves as a concrete example of a cross-platform mobile app created with Capacitor.

![Dark Dolores](https://framagit.org/yphil/assets/-/raw/master/img/Dolores/screenshot-01.png "Dark Dolores")

![Light Dolores](https://framagit.org/yphil/assets/-/raw/master/img/Dolores/screenshot-02.png "Light Dolores")


## The Adventure of Alternative Platforms
In the past, alternative platforms like Palm Pilot or later Nokia Maemo attempted to disrupt native mobile app development by using languages like JavaScript and HTML. Unfortunately, they eventually faded away due to "various obstacles and limited market adoption"—that's a polite way to describe what actually happened. I was *there*—but that's not the focus anymore.

## Give Them What They Deserve, Remove OOP from Your Resume
Traditionally, mobile development was associated with object-oriented programming (OOP) and the belief that "you can't create a graphical interface without OOP." Without realizing that JavaScript is inherently object-oriented—heck, even functions are objects in JS. With modern frameworks and tools, web developers can now create stunning user interfaces without writing a single line of Java. The problem isn't objects, inheritance, classes, or even the JVM—the problem is Java itself.

## Capacitor: Making the Connection
A significant advancement in this domain is Capacitor, a powerful open-source framework that allows developers to create cross-platform mobile applications using web technologies. Capacitor is built on Apache Cordova (previously known as PhoneGap) and is [now maintained by the Ionic team](https://ionic.io/resources/articles/what-is-apache-cordova). It acts as a bridge between web technologies and the native features of mobile devices.

![Capacitor](/images/capacitor.webp "Capacitor")

### Workflow: From Web to Mobile App with Capacitor
The process of creating mobile applications with Capacitor follows a simple workflow that leverages your existing web development skills. Here's a brief overview:

1. Code your application using HTML, JavaScript, and CSS, just like you would for a web application.
2. Install the Android SDK using [Android Studio](https://developer.android.com/studio) and set up your environment.
3. Install Capacitor, add platforms, and sync your code—on the first launch, it will simply copy everything: HTML, CSS, JS, images, and it may even fetch code from your CDNs to package it all up.
4. Use [Capacitor's APIs](https://capacitorjs.com/docs/apis) to access native device features like the camera, geolocation, or notifications.
5. Customize the appearance and behavior of your application using standard frameworks like [Bulma](https://bulma.io/) ; #tip Create a custom font with *juste* the right icons using [Fontello](https://fontello.com/);
6. Test your app using Capacitor's development server or by running it directly on physical devices or emulators. I couldn't get Android Studio's emulator to work—oh well, when you're developing a web app, you don't need an emulator anyway 😎
7. Deploy your application to app stores and/or distribute it as an APK bundle for direct download. You can also distribute it as a web application.

### Environment

``` shell
export ANDROID_HOME=~/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/platform-tools/
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/latest/bin/
export PATH=$PATH:$ANDROID_HOME/emulator/

export JAVA_HOME=~/bin/android-studio/jbr
export CAPACITOR_ANDROID_STUDIO_PATH=~/bin/android-studio/bin/studio.sh
```
### Create / import the project

``` shell
cd www
npm i @capacitor/core
npm i -D @capacitor/cli
npx cap init
npm i @capacitor/android @capacitor/ios
npx cap add android
npx cap sync
```

### Build!
In the shell:
``` shell
npx cap build android
```

In Android Studio:
``` shell
npx cap open android # hence $CAPACITOR_ANDROID_STUDIO_PATH
```
- Menu "Build" / "Generate Signed Bundle / APK";
- Create a package signing key
- Choose "release"
- The installed APK is located at android/MonProjet/release/MonProjet-release.apk
### Desktop compatible
And of course, your app remains usable on the desktop:
``` shell
xdg-open www/index.html
```

## Smell the coffee
The days when mobile development required blood, sweat, and tears are long gone. Thanks to the emergence of tools like Capacitor, web developers can now venture into mobile development with confidence. If, like me, you've been coding increasingly powerful web apps for decades, you already have a solid foundation. Embrace this new power. You are a mobile developer 😎
