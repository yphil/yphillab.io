---
title: Current window name in i3status
date: 2022-10-12 02:16:28
thumbnail: /images/i3-window-title-in-status.png
tags: 
- i3
- python
---

<style>

.imagediv {
    position:relative;
    display: inline-block;
}

@keyframes animate {
    0% {
        opacity: 0;
    }

    50% {
        opacity: 0.7;
    }

    100% {
        opacity: 0;
    }
}
		
.imagediv::after {
	animation: animate 5s ease infinite;
    content:"↘️";
    position:absolute;
    left:150px;
    bottom:15px;
}

</style>

Given the very tiling nature of i3, the name of the current window can be hard to impossible to decipher, particularly those looooong-ass web page titles.

[The following Python script](https://framagit.org/yphil/dotfiles/-/blob/master/.scripts/px-i3-window-name.py) gets said title - doesn't choke when there is none - and echoes it to a one-line file that can be in turn read by i3status.

<div class="imagediv">
	<img src="/blog/images/i3status.png">
</div>


See `~/.config/i3/i3status.conf` below.

``` python ~/.scripts/px-i3-window-name.py
#!/usr/bin/env python3

import os
import i3ipc
from html import escape

TMPFILE = os.path.expanduser("/tmp/.current_window_name.txt")

i3 = i3ipc.Connection()

thisId = 0

root = i3.get_tree()

def writeTmpFile(wname):
    if wname:
        wname = escape(wname)
    else:
        wname = 'Unnamed window'
        
    with open(TMPFILE, 'w') as the_file:
        the_file.write(wname)
        
def on_window(i3, e):
    global thisId
    if e.change == "focus":
        thisId = e.container.id
        writeTmpFile(e.container.name)
    if e.change == "title":
        if e.container.id == thisId:
            writeTmpFile(e.container.name)

def on_workspace(i3, e):
    if e.change == "focus":
        writeTmpFile("Workspace " + e.current.name)
    
i3.on('window', on_window)
i3.on('workspace', on_workspace)

i3.main()
```
## in `~/.config/i3/i3status.conf`

``` c++ ~/.config/i3/i3status.conf
order += "read_file current_window_name"

read_file current_window_name {
    format = "<span foreground='#ff8c00'>%content</span>"
    format_bad = ""
    path = "/tmp/.current_window_name.txt"
}
```

## Remember to install i3ipc

``` shell
python3 -m pip install i3ipc
```
NB if you do things [the right way](https://framagit.org/yphil/dotfiles), you don't have to remember all of that. Everything above is automatically installed on each and every of my new machines.

Stay tuned for more about just that - automatic deployment and maintaintance using a single, simple shell script instead of mastodons like [Guix](https://www.youtube.com/watch?v=iBaqOK75cho) - and more.
