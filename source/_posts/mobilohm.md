---
title: Introducing MobilOhm
date:  2023-08-04
tags:
- mobile
- JVM
- JS
---

## My apps needed a Ohm

MobilOhm is [a collection / suite of apps](https://mobilohm.gitlab.io/) to slip away from the GAFAM stranglehold.

[![MobilOhm logo by Me](https://yphil.frama.io/mobilohm/img/big-logo.png)](https://mobilohm.gitlab.io/)

## What
A collection of apps without bloat or ads.


- [Dolores](https://mobilohm.gitlab.io/dolores/), A workout timer to gloat over your stats, drenched in sweat on the seaside
- [Papiers](https://framagit.org/yphil/papier), A note-taking app that syncs in the cloud and does'nt phone home doing so
- [Todora](https://framagit.org/yphil/todora), A laundry / shopping list just because (will sync too, soon)
- [Pétrolette](https://petrolette.space/), A news page that is basically your home on the internet, to inform your way around the rampant censorship
- And there's [way more](https://framagit.org/yphil/) where that comes from :)

[![mobilohm-apps-x-card.png](https://mobilohm.gitlab.io/img/mobilohm-apps-x-card.png)](https://mobilohm.gitlab.io/)

You can install and run (yes, offline too) each - well, most - of those apps both on your Desktop(s) and you phone(s) that's the idea. They use [the RemoteStorage protocol](https://remotestoragejs.readthedocs.io/), and you can even host your own instance.

### RemoteStorage

- [Documentation](https://remotestoragejs.readthedocs.io/)
- Review your [current apps and authorizations](https://rs.5apps.com/apps)
- Acces your [data](https://inspektor.5apps.com/) online

## How

If you find any of the MobilOhm apps helpful, and would like to support its development, consider making a contribution through [Ko-fi](https://ko-fi.com/yphil) or [Liberapay](https://liberapay.com/yPhil/).

Your support helps keep those apps Free, Libre, Open Source, and ad-free.



## Why

Aren't there a ton of similar stuff in the app stores?

Short answer? **No** ; Take [Dolores](https://mobilohm.gitlab.io/dolores/) for instance, I dare you to find a streamlined, maintained, ad-free app that can do half of what it does. Viewed from here and now the "Google Play Store" (silly British comedic voice, read on) is a steaming heap of bloated, buggy and ad-ridden malware. Yeah I'm exaggerating a tiny bit but that's what I do and you probably know what I mean.

## 3 apps in 3 weeks

[I know](https://yphil.gitlab.io/2023/07/16/dolores-en/), right?

## App Stores

Yes, of course the apps will be available in *at least* one store ; I hear one has to pay to see their **free** apps on the Google repulsively named P*l*ay Store, so that's for the birds - and talk about bad company - but yeah, [F-Droid](https://f-droid.org/), soon. Thing is, I can't even *install* it on my Android 9 phone 🤨

For now use your fairy dust to install & upgrade the apps, me I just plug the phone into my rig and use a file manager. Yeah, I said I *know*!
