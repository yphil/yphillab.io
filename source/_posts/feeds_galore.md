---
title: Twitter, Rumble, RSS galore!
date: 2023-05-16 14:13:05
tags: 
- pétrolette
- shell
---

## At last, Rumble RSS feeds!!

Rumble has been very ~~stupid~~ stubborn about not providing the RSS feed of their user's channels. This is so inane and foot-shooting that it doesn't even deserve a debate ; I myself have been using my own RSS scraping & serving system - that I noticed nobody cared to ask me about - to get Rumble feeds, but you can have them in your Pétrolette too now, thanks to this company [OpenRSS](https://openrss.org/), that acts as a proxy to 

- Fetch the (in this case, Rumble) page
- Build a feed from it (which is more complicated than it sounds) and 
- Serve it.

The way you use it is thusly ; you take the URL of the page that doesn't have RSS, like for instance https://rumble.com/c/StevenCrowder and take out the protocol, like so: `rumble.com/c/StevenCrowder` and then prefix it with `https://openrss.org/`, and there you go your RSS feed's URL is `https://openrss.org/rumble.com/c/StevenCrowder`. Give it to Pétrolette and Bob's your mother's brother.

![Note the icon & links (see the last #pétrolette entry)](/images/socials.png)


## Twitter, too!

This one is better because it's a bonafide FLOSS (🇫🇷!) company (and not a little weird "nonprofit organization funded only by altruistic donors to provide free RSS feeds to the public" [hum](https://openrss.org/about)) that provides a FULL Twitter alternative frontend, augmented with... Rss feeds! Use it like this: Take the username you want to follow, like @elonmusk (just a random example) and stick it to the end of your favorite [Nitter](https://nitter.net/) [instance](https://github.com/zedeus/nitter/wiki/Instances), like this: `https://nitter.c-r-t.tk/elonmusk` then add the rss suffix: `https://nitter.c-r-t.tk/elonmusk/rss` and voila 🙂

## No RSS, really?

Turn your brain on for one second and realize that this latest trick is ideal to bypass and workaround any "no RSS here" policy, because every reoccuring, regularly publishing source has a Twitter feed and **BAM** free RSS feed.

### Should we integrate it?

Maybe a checkbox in the "add feed" dialog to use alternate sources:

- `https://rumble.com/c/user` => `https://openrss.org/rumble.com/c/user` ;
- `https://twitter.com/user` => `https://nitter.c-r-t.tk/user/rss` ;
- Etc.

And a config option in the menu to use your favorite instance, like we already do for the searx RSS feeds.

What do you say? And when exactly are you going to [pay me for it](https://liberapay.com/yPhil/)? My plumber wants to now.


## Are your feeds alive?

I made a [quick-and-(very)dirty script](https://framagit.org/yphil/petrolette/-/blob/master/test/feeds-test.sh) to check if your feeds are still alive, in order to routinely check the 300+ (and counting) pétrolette default feeds. You can use it to check your feeds file, too.

I may integrate it to the "official" test suite if I can be bothered / (payed? The subscriptions are down to virtually nothing nowadays, how exactly am I supposed to continue serving you guys?) it would be much more pretty.
