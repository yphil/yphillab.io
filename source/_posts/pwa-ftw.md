---
title: Progressive Web Apps - For The Win
date:  2023-08-11
tags:
- mobile
- JS
---

## PWA FTW!

Ok, scratch [this](https://yphil.gitlab.io/2023/07/16/dolores-en/), scratch ~~everything~~.

[Dolores](https://mobilohm.gitlab.io/dolores/) is now a [Progressive Web App](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps), available on [the (new URL) MobilOhm website](https://mobilohm.gitlab.io/).

![](https://www.w3.org/html/logo/downloads/HTML5_Badge_256.png)

After about a month of Android APK development, I'm not impressed. I mean even [Capacitor is still way too much Java](https://capacitorjs.com/docs/apis) for my blood:

- Gradle (and its delicious not-really-java syntax) which is broken (just a bad symlink, but still) on Debian BTW
- Maven / Ant 😐
- Docker (! yes, to test your APK against all kinds of cryptic stuff for inclusion in the app stores)
- Vagrant (yes, [that Vagrant](https://chaos.social/@zhenech/110869288240298587))
- Android Studio (yes, the Behemoth you need to at least generate the APK signing keys anyway)
- Etc.

And after my build toolchain suddenly stopped working after a system install (no way to sign a release w/o resorting to Android Studio) I decided to stop spitting coffee at my screen and give the DJinn whispering "PWA FTW" in my inner ear more of my attention.

I passingly tested Google [WorkBox](https://developer.chrome.com/docs/workbox/) which turned out to be a giant mess, so I wrote my own Service Worker and it service worked right away.

Go ahead and [test her](https://mobilohm.gitlab.io/dolores/), [feedback welcome](https://framagit.org/yphil/dolores/-/issues).
