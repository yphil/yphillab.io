---
title: Pétrolette 1.5 - Bougies neuves 🇫🇷
date: 2022-03-12 15:02:24
tags: pétrolette
---

[Pétrolette](https://petrolette.space/) est une page d’accueil de lecture d’actualités, [libre](https://framagit.org/yphil/petrolette/-/blob/master/LICENSE). Elle est immédiatement utilisable **sans inscription** avec la même adresse (URL) dans le navigateur d'un ordi, d’un appareil mobile, ou encore d'une télévision, nous y reviendrons.
Cette année, le projet fête ses dix ans :) 🎉

![Audio (et vidéo) directement dans Pétrolette](https://i.postimg.cc/PxLNwQ3T/screenshot-2022-03-12-21-04-09-1920x1080.png)

<!-- toc -->

![Pétrolette](https://img.linuxfr.org/img/68747470733a2f2f692e706f7374696d672e63632f537336506435384e2f706574726f6c657474652d312d332e706e67/petrolette-1-3.png)

# Rappel des fonctionnalités

- Pas de pub, pas de traceurs, pas d’identification, juste les actus ;
- Sauvegarde des préférences dans le cache du navigateur, persistante
- Synchronisation (optionnelle) en ligne (Cloud) ;
- Lecture de tout type de flux (Atom, Media-RSS, etc.) ;
- Lecture audio et vidéo directement dans Pétrolette ;
- Découverte de *tous* les flux d’une page / un site web à partir de l’URL ;
- Recherche dans les flux ;
- Indication du nombre de nouveaux articles ;
- Lien direct vers les ressources de l’article (image, vidéo, son, etc.) par ex. pour ouverture dans un lecteur externe ;
- Gestion avancée des images ;
- Import / export du fichier de flux ;
- Construction de « flux de recherche », moteur de recherche configurable ;
- Interface (affichage) mobile / responsive : adaptée à tout type d'écran ;
- Entièrement pilotable au clavier
- Bookmarklet / Bouton navigateur pour ajout dans Pétrolette d’un site web à la volée ;
- Compatible avec les navigateurs libres : fichiers exécutables non compressés liés directement, licences disponibles et au format standard.
# Nouveautés

Depuis [la dernière version](https://linuxfr.org/news/petrolette-v1-4-plus-simple-plus-rapide), beaucoup de choses ont changé (voir [le journal des changements](https://framagit.org/yphil/petrolette/-/blob/master/CHANGELOG.md) pour les détails) qui ne sont pas visibles immédiatement, mais pour les plus grosses nouveautés :

## Découverte de tous les flux trouvés dans une page

C'est une demande qui revenait régulièrement, dans [les tickets de support](https://framagit.org/yphil/petrolette/-/issues) ou ailleurs: Pétrolette (plus précisément [Feedrat](https://www.npmjs.com/package/feedrat), la bibliothèque chargée de ce travail) ne renvoyait que le dernier flux trouvé ; elle présente maintenant une liste des flux rencontrés à l'URL / adresse du site, permettant de les consulter et de les ajouter à l'onglet courant.

![screenshot-2022-03-12-15-22-43-958x1039.png](https://i.postimg.cc/cLcXhtcT/screenshot-2022-03-12-15-22-43-958x1039.png)

## Suggestions de flux par catégorie

[Le dépôt de code](https://framagit.org/yphil/petrolette) contient [un fichier](https://framagit.org/yphil/petrolette/-/blob/master/public/js/default-feeds.json) de près de 400 flux RSS / Atom classés par catégories et mis à jour régulièrement qui sert pour la première utilisation afin de présenter aux pilotes novices un grand choix de flux pour commencer à utiliser Pétrolette facilement. Du coup, beaucoup ne voient jamais ces flux (à moins bien sur de cliquer sur "Réinitialiser", sans oublier bien sur de cliquer sur "Sauver" avant) et il est finalement assez fastidieux d'ouvrir un onglet en "navigation privée" juste pour accéder auxdits flux, ou encore de sauver son fichier de flux, puis réinitialiser Pétrolette, importer le fichier et choisir "Fusionner" ouf ; ils sont maintenant accessibles directement dans la fenêtre de dialogue "Nouveau flux", représentée ci-dessus.

![Suggestions de flux](https://i.postimg.cc/QMKL4Q86/screenshot-2022-03-13-11-38-33-1121x1039.png)

## Zappeuse

Jusqu’à il y a peu, Internet comportait principalement deux types d'usage, au sens strictement matériel : en utilisant un ordinateur - de bureau ou portable, et un appareil mobile, téléphone ou autre bidule de ce genre.
Depuis quelques années, disons à l'apparition des _Boxes_ de seconde génération avec leur télécommande, on remarque un troisième type de consultation du net : sur la grosse TV du salon, vautré sur le canapé. Les personnes les plus _smart_ branchent cette dernière sur un ordinateur, la transformant en super écran, les autres se contentent de la connecter directement au réseau.
Pétrolette est maintenant adaptée à cet usage, le texte bien lisible et les dialogues contrastés, et est complètement pilotable au clavier, facilitant l'usage de ces petites choses à dent bleue qu'on trouve partout à vil prix.

![Keyboard-complete](https://i.postimg.cc/6qX4R18n/cap.png)

💡 **Pour mettre Pétrolette au premier plan à tout moment**

- Garder Pétrolette dans le premier onglet, idéalement "épinglé" pour qu'elle y reste entre chaque redémarrage ;
- Attacher cette commande à un raccourci clavier pratique : `setxkbmap -synch ; xdotool search 'Mozilla Firefox' windowactivate sleep 0.5 key --clearmodifiers alt+1 sleep 0.5 key t`

L'onglet courant est immédiatement activé et la navigation possible avec les touches fléchées `gauche` / `droite`, et les touches `home` / `end`.

# Détails techniques

## Sécurité de l'instance

Les règles de [CSP](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) ont été affinées au maximum pour permettre le maximum de sécurité dans une appli dont la nature même est de lire n'importe quel type de contenu venu de n'importe où ; Ces règles sont maintenant plus faciles à personnaliser.

## Cuisine et dépendances

Depuis longtemps les dépendances du serveur sont installées avec [NPM](https://www.npmjs.com/) ([package.json](https://framagit.org/yphil/petrolette/-/blob/master/package.json)) et celles du client à l'aide de [Bower](https://bower.io/), qui est maintenant obsolète ; j'ai envisagé un moment [Yarn](https://yarnpkg.com/), mais pour le moment j'ai tout porté sur NPM (avec [une route dédiée pour chaque lib](https://framagit.org/yphil/petrolette/-/blob/master/petrolette.js#L48)) en attendant d'avoir les ressources pour me débarrasser de NPM et développer une solution de gestion de dépendances basée directement sur les dépôts VC, avec quelque chose comme [Gulp](https://gulpjs.com/).

# Dix ans, quand même

![screenshot-2022-03-12-15-04-03-958x1039.png](https://i.postimg.cc/B6HHndtb/screenshot-2022-03-12-15-04-03-958x1039.png)

Le projet est né vers la fin des années 2000, sous la forme d'une page PHP qui lisait un unique fichier `feeds.xml` directement sur le serveur. Une autre page permettait l'ajout et le déplacement des flux, tout ça s'appelait NWS, était hébergé sur Github, qui venait d'ouvrir (disons surtout que je venais d'apprendre à utiliser Git) et avait fait l'objet de [l'un de mes premiers messages là](https://linuxfr.org/forums/general-cherche-logiciel/posts/cherche-un-rss-reader-en-php-avec-page-publique#comment-1321606).
Le projet a ensuite migré sur Bitbucket vers 2014, pour finalement atterrir sur Framagit, l'instance Gitlab des bons chatons de Framasoft.

Cette facilité de Git à permettre l'import / export de toute l'historique d'un projet a permis d'en conserver toute la trace jusqu'ici, ce que je trouve vraiment impressionnant ; l'année dernière je me suis amusé à générer [une vidéo](https://exode.me/w/s5aVEtmxkDfXQ9TEBk3WNx) avec [Gource](https://github.com/acaudwell/Gource) à partir des sources mais elle fait plus d'une demi-heure, c'est insensé :)

Pétrolette est maintenant [une application NodeJs](https://framagit.org/yphil/petrolette/-/blob/master/DEV.md#under-the-hood) qui est son propre serveur. La prochaine version, en cours de développement, est écrite dans un tout autre langage, c'est pour ça que ça prend du temps mais je n'en dis pas plus car c'est une surprise.

## Dans les cartons

La prochaine version sera installable en version "isolée" qui lira et écrira un unique fichier de flux cette fois sur le serveur, pour faciliter les usages strictement perso et dans des conditions spéciales, comme derrière des protections réseau d'entreprise, des "noScript" interdisant l’accès au [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage), etc.

## À l'aide

Entre l'hébergement (dans le nuage Ikoula) le nom de domaine (chez Gandi) de l'instance publique, et temps de développement / maintenance (la nuit) le projet est loin de joindre les deux bouts.
Les personnes utilisant Pétrolette (surtout depuis dix ans 🙂) et souhaitant aider le projet peuvent le faire de trois manières ; par ordre de préférence :

- Don récurrent [chez Liberapay](https://liberapay.com/yPhil/)
- Don ponctuel ou récurrent [chez Ko-fi](https://ko-fi.com/yphil/)
- Don ponctuel [chez Paypal](http://paypal.me/yphil)

Merci beaucoup à vous.

# Conclusion

En 10 ans j'ai développé un certain nombre de logiciels, mais aucun ne m'a servi autant que Pétrolette, tous les jours, tous-les-jours depuis 10 ans.

Je _sais_ (je reçois souvent de gentils courriers abondants de belles histoires) que Pétrolette aide _beaucoup_ de gens à s'informer, et j'en suis vraiment **très** heureux.


- [Pétrolette](https://petrolette.space/)
- [Code source](https://framagit.org/yphil/petrolette)
- [Support](https://framagit.org/yphil/petrolette/-/issues)
- [CHANGELOG.md](https://framagit.org/yphil/petrolette/-/blob/master/CHANGELOG.md)
- [Tag #pétrolette sur LinuxFr](https://linuxfr.org/tags/p%C3%A9trolette/public)
- [Aider développement & maintenance](https://liberapay.com/yPhil/)


Merci de votre attention, et à la prochaine version !
