---
title: Filmer, monter, encoder, diffuser - Why, Phil? 🇫🇷
date: 2016-07-07 15:13:40
thumbnail: whyphil01-thumbnail.jpg
tags:
- video
- audio
---

## Why, Phil?

Est une nouvelle série de Webisodes courts, doux et à propos (short, sweet and to the point) qui entre deux notes de guitare, présentent les outils que j'utilise en studio, enfin, pour le moment c'est ça le sujet. Outils, techniques, trucs et astuces, et plus.

![Why, Phil?](/images/whyphil01-thumbnail.jpg)

<!-- toc -->

Dans le 1er épisode, on fait connaissance avec [Qtractor](http://libremusicproduction.com/tutorials/qtractor-quickstart), le centre solaire de mon studio, et néanmoins une étoile majeure de la galaxie Linux audio. Tuwois, Qtractor est un logiciel unique en son genre, qui au contraire de tous ses collègues, refuse de devenir une galaxie à lui tout seul, je m'explique.
Quelque soit l'OS, à un moment la tentation est grande de tout gérer en interne, de développer un moteur audio, un moteur MIDI, un toolkit, des source (instruments) des traitements, etc. Qtractor, qui au passage, est traditionnellement le plus avancé des séquenceurs sous Linux - il faisait du MIDI+Audio en 2007, alors que le MIDI n'est arrivé dans Ardour qu'en 2014, et a mis des mois à se stabiliser, et n'est pas encore techniquement finalisé, il gère tous les formats de plugins audio à mesure qu'ils évoluent, etc - tient à rester une brique de la cathédrale, et à utiliser préférentiellement les libs qui composent l’écosystème, à tous les étages du traitement. Rui Nuno Capela, son auteur (et de [plein d'autres projets](http://www.rncbc.org/drupal/node/13)) a coutume de dire "[Qtractor ne touche pas au son](https://media.ccc.de/v/minilac16-yetsameoldqstuff)". Tant en termes de code que d'expérience utilisateur, cette approche a la légèreté et l'élégance d'une chanson de Neil Tennant.

On peut reprocher à Qtractor son approche "tout-Qt" (aucun widget propriétaire, que du standard, so 2005 un peu sur les bords) ses multiples fenêtres et mille autres idiosyncrasies - Ce logiciel a une vraie _attitude_ - en attendant j'ai fait deux albums avec (et 20 épisodes d'une émission de radio) et je compte pas m’arrêter là. J'aimerais vraiment bien que les gens voient la lumière, sur ce coup : Qtractor est la meilleure chose arrivée à la musique depuis la Lexicon 480L. Et aujourd’hui que tu peux mettre [la seconde](http://www.housecallfm.com/download-gns-personal-lexicon-480l) dans [le premier](http://qtractor.sourceforge.net) sans faire péter une session de 500$/h, le monde _doit_ savoir.

![Why, Phil?](/images/whyphil10-thumbnail.jpg)


## Season One

- [Why, Phil? #01 1st steps](https://www.youtube.com/watch?v=8Q0PyzzDjUY)
- [Why, Phil? #02 Kick & the Snare](https://www.youtube.com/watch?v=2-nGhlNRSd0)
- [Why, Phil? #03 Sessions & Busses](https://www.youtube.com/watch?v=eGvsFs6DD9Y)
- [Why, Phil? #04 From Loop to Song](https://www.youtube.com/watch?v=qq-TySzCQqU)
- [Why, Phil? #05 Telecasting a Guitar Track](https://www.youtube.com/watch?v=2zZO7-wo2_o)
- [Why, Phil? #06 RockBand Drums in DrumGizmo](https://www.youtube.com/watch?v=9E9QoEzr2I8)
- [Why, Phil? #07 Angus Young goes to Church](https://www.youtube.com/watch?v=cC0GNYJcL4I)
- [Why, Phil? #08 A Song is Cooking](https://www.youtube.com/watch?v=SHKq8AyqwnA)
- [Why, Phil? #09 Automata](https://www.youtube.com/watch?v=iHUwlmuFjq8)

Truc marrant, c'est qu'au long des épisodes et tutoriels, bien forcé d'improviser des bidules pour démonstrationner le bouzin, je me suis retrouvé à écrire une chanson "Claire" ou est-ce le contraire, et pour autant que je sache c'est relativement inédit, cette façon de faire. Elle sera en bonus de "Faux Frogs" si elle est jamais finalisée, les chansons mènent une vie bien à elles. C'est surtout que Claire ne l'a pas entendue encore, en fait :|

## Et donc, pourquoi ?

Ben, parce que mes outils, mes habitudes, mes techniques, sont très peu documentées. Spécifiquement, Qtractor, et la pléthore de plugins que j'utilise quotidiennement, souffrent d'un manque cruel de vidéos en ligne, ce qui ne remplace certes pas un bon manuel (j'ai pour habitude de participer aux docs des outils que j'utilise, une fois que j'ai compris comment ils marchent) mais reste le moyen le plus populaire de prise de contact avec un environnement logiciel complet (Penser Blender, Ardour, Gimp, Inkscape, voire carrément Linux, etc.) et puis, je l'utilise depuis si longtemps que je considère ça comme ma contribution à son développement. Notez qu'au matin de shooter la vidéo "Automation" je n'avais encore jamais mis en œuvre la technique dont je fais la démonstration, mais ça s'est réglé en [un message à l'auteur](http://www.rncbc.org/drupal/comment/7056#comment-7056).

Beaucoup (plein) de musiciens sont encore enfermés dans leurs écosystèmes respectifs, et flippent dés qu'il est question de changer d'OS, car ça veut dire à peu près tout changer à part la chaise. [Nous](http://www.linuxaudio.org/), on veut leur montrer ce qu'ils gagnent à franchir ce pas :

- Des logiciels innovants, libérés de contraintes commerciales hors-sujet, et qui sonnent ;
- qu'il est possible de modifier en profondeur en participant au développement, à quelque niveau que ce soit ;
- Utilisables tout de suite [en version récente](http://kxstudio.linuxaudio.org/Repositories) ;
- une vraie communauté [d'utilisateurs](http://lists.linuxaudio.org/pipermail/linux-audio-user/) et de [développeurs](http://lists.linuxaudio.org/pipermail/linux-audio-dev/) ;
- un système modulaire, puissant et bidouillable.

Les retours [sont bons](https://linuxmusicians.com/viewtopic.php?f=1&t=15818&sid=1890123037ac5290d348c17e3de23a3e), la presse [est unanime](http://libremusicproduction.com/news/20160620-check-out-why-phil-new-linux-audio-webshow-series) :)

![Why, Phil?](/images/whyphil04-thumbnail.jpg)


## How, Phil?

Write, shoot, derush, edit, encode; rinse & repeat ; c'est ce que je _fais_, maintenant ; Je suis devenu une régie complète de production vidéo, après l'audio et sa distribution. Un épisode de "Why, Phil?" c'est entre 5 et 10 minutes de vidéo + audio, et c'est une journée (tranquille, hein, mais quand même) de taf en moyenne. Bien sur, chaque vidéo est meilleure et plus rapide à faire que la précédente, en raison des microscopiques idiosyncrasies du métier qui rentre à chaque itération. Et la musique, et les vidéos des chansons en même temps.

### Patreon

Des gens, intéressés par mon travail, souhaitent le voir continuer et m'aident financièrement, c'est pas cool, ça ? Merci, les gens, sans vous ce serait (vraiment) pas pareil ; Ça aussi, ces moyens de financement, c'est nouveau. Je prépare un album ~~assez~~ différent du précédent. Un album de rock moderne, Basse/Batterie/Guitare/MS20 (enfin synthé monophonique, quoi) de la disto paramétrique dans du tremolo (ou l'inverse, le combo qui fera le plus de bruit) avec de la mélodie et du sens, tu voyez le genre. De nouveaux musiciens, de nouveaux techniciens même, enfin de nouvelles rencontres. Ça va être énorme.

### LA chaîne graphique qui va bien
Je tiens à remercier chaudement Blender, Inkscape, et Gimp, fidèles (et stables) compagnons de route, sans qui rien de tout ça n'aurait été même envisageable.

## Qu'est que tu racontes?
Oui, c'est en anglais. Le monde a besoin de savoir, pas juste la Gaule ; voyez ça comme ça : vous n'aurez probablement aucun mal à comprendre mon angliche d'aéroport, alors que si je fais cette série en français, comme dit Goossens dans "Georges et Louis romanciers", "ça va me faire cinq lecteurs" :)

## Et maintenant?
Quand j'aurais fait le tour de Qtractor, j'envisage de faire quelques tutos sur les outils graphiques que j'utilise, spécifiquement mes techniques de montage vidéo (que j'ai apprises en regardant [d'autres videos](https://www.youtube.com/channel/UCY2G7EknC5BJeVpZU3Iperg) :p) et d'autres making-of des vidéos à venir (j'en ai trois en retard, les tournages sont fait, sans compter l'album à venir) dans une mise en abyme connectée toujours plus vertigineuse !

Au plaisir ; Keep on rocking the free world ♫
