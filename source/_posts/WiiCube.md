---
title: WiiCube
date: 2025-02-06 01:10:27
tags:
- JS
---

## Overview
[WiiCube](https://gitlab.com/yphil/wiicube) is a **game** browser and **launcher** designed to work with **any emulator**.

![wiicube](https://framagit.org/yphil/assets/-/raw/master/img/wiicube.png)

## Features
### Game controller support
Navigate and launch games using a keyboard, mouse, or **game controller**.
### Game covers (seamless) download
Download game covers in one click ; **No API key or login required**.
### Cross-Platform
Compatible with Linux, Linux, and **Linux**.

## Install / Run
Two options:
### Download Pre-Built Releases (Recommended)
[Download & install the appropriate package](https://gitlab.com/yphil/wiicube/-/releases) for your operating system ([AppImage](https://appimage.org/) packages run basically everywhere).

### Run / build from Source
1. Clone the repository:
   `git clone https://gitlab.com/yphil/wiicube.git`
2. Navigate to the project directory:
   `cd wiicube`
3. Install the required dependencies:
   `npm install`
4. Start WiiCube:
   `npm start -- --no-sandbox --help`
5. Build WiiCube:
   `npm run build`

## Usage

### ⚠️ Important
- When running WiiCube on the command line, or in a script (as one does, I myself have it [bound to joypad "square"](https://framagit.org/yphil/dotfiles/-/blob/master/.scripts/px-controller-listener.sh) on my DualShock 4) ; **use the** `--no-sandbox` [argument](https://www.electronjs.org/docs/latest/tutorial/sandbox) ; This is a [well-known caveat](https://github.com/electron/electron/issues/18265) of [the chrom[e | ium] engine paradigm](https://chromium.googlesource.com/chromium/src/+/HEAD/docs/design/sandbox.md#sandbox-policy):
  - `wiicube --no-sandbox`

- If you launch Wiicube with [the menu entry, the icon or the AppImage](https://gitlab.com/yphil/wiicube/-/releases), don't worry **this doesn't apply**. I [saw to it](https://gitlab.com/yphil/wiicube/-/blob/master/build.sh?ref_type=heads) 😐.


At first launch, WiiCube will prompt you to:
1. **Set Games Directory**: Specify the path to your games directory.
2. **Set Emulator Path**: Provide the path to your emulator executable.
2. **Set Emulator Arguments / flags**: Additional (and optional) arguments to pass to the emulator. For example: `--fullscreen --no-splash` ; For Dolphin, useful arguments are:
- `-e <file>, --exec=<file> Load the specified file`
- `-b, --batch              Run Dolphin without the user interface (Requires --exec)`

### Control
WiiCube supports both game controller, keyboard and mouse for navigation and interaction.

#### Game Controller Controls

D-Pad only:

- ← `Left`: Navigate to the previous game in the gallery.
- → `Right`: Navigate to the next game in the gallery.
- ↑ `Up`: Scroll up in the game list.
- ↓ `Down`: Scroll down in the game list.

| PlayStation | Xbox | Nintendo | SNES | Sega Genesis | PC Gamepad   | Action               |
|-------------|------|----------|------|--------------|--------------|----------------------|
| 🟥 Square   | X    | Y        | X    | X            | Button 3 (X) | Does nothing for now |
| 🔼 Triangle | Y    | X        | Y    | Y            | Button 4 (Y) | Does nothing for now |
| ❎ Cross    | A    | B        | B    | A            | Button 1 (A) | **Launch the game**  |
| ⭕ Circle   | B    | A        | A    | B            | Button 2 (B) | **Quit WiiCube**     |


#### Keyboard Controls

- ← `Left`: Navigate to the previous game in the gallery.
- → `Right`: Navigate to the next game in the gallery.
- ↑ `Up`: Scroll up in the game list.
- ↓ `Down`: Scroll down in the game list.
- `Enter / Return`: Launch the selected game.
- `Esc`: Exit WiiCube.

## #Todo
Platform selection. For multiple instances. I think. Right now it's basically only set to display and launch Nintendo's 7th generation consoles, namely GameCube & Wii ; I'm still thinking of a way to make multi-platform easy ; This would be easily sorted out with CLI args (and then I can map ⭕ `circle` to [pcsx2](https://pcsx2.net/) and 🔼 `triangle` to... [UAE](https://fs-uae.net/)? [Redream](https://redream.io/)? Flycast?) let us think this "multiplatform" thing through.

## Contributing & helping
- Contributions to WiiCube are welcome.
- WiiCube is [Free, Libre, and Open-Source Software](https://gitlab.com/yphil/wiicube/-/blob/master/LICENSE). However the development requires [a lot of time](https://www.youtube.com/watch?v=JlbMEx9H6FE) and a lot of work. In order to keep developing it with new features I need your help ; Please consider to support the WiiCube project by [sending a donation](https://yphil.gitlab.io/ext/support.html) ; Even the smallest amount will help *a lot*.

## License
WiiCube is released under the GPLv3 License. See the [LICENSE](https://gitlab.com/yphil/wiicube/-/blob/master/LICENSE) file for more details.

*Icon by yPhil & Stable Diffusion*

## Contact
For questions or feedback, you can reach out to [me (yPhil) via by blog](https://yphil.gitlab.io/).
