---
title: Emacs 24 - Toute résistance est inutile 🇫🇷
date: 2013-09-18 19:19:07
tags:
- lisp
---

Emacs 24 [vient de sortir](https://lists.gnu.org/archive/html/info-gnu-emacs/2012-06/msg00000.html) (1 an Emacs = 1 instant terrestre) c'est l'occasion de s'approcher un peu plus près.
Le premier contact avec Emacs est toujours un peu brutal. Voici deux ou trois choses qui peuvent aider à répondre à la question "pourquoi utiliser Emacs en 2017 ?" comme ça tu seras prèt. À la fin tu pourras essayer ma config de ninja.

Hors de la boite
-----------
De base, Emacs se comporte bizarrement. Pourtant toute la puissance qui va être mise en oeuvre plus tard est bien là, brute. Certains disent qu'Emacs est un OS avec un éditeur de texte chelou, et si c'est drôle c'est sans doute parce que c'est vrai. Les concepteurs d'Emacs, et maintenant les mainteneurs, ont fait le choix d'un système qui définit ses propres règles, ses propres paradigmes. Emacs est écrit en C, mais toute la couche "userland" est en LISP, enfin un dialecte LISP, appelé [Emacs Lisp](https://www.gnu.org/software/emacs/manual/elisp.html) ([intro](https://www.gnu.org/software/emacs/manual/html_node/eintr/index.html)). Emacs est ex-ten-sible.

### Conventions
- `C`   : (Control) la touche Ctrl
- `M`   : (Meta)    la touche ALT
- `S`   : (Shift)   la touche Maj.
- `s`   : (Super)   la touche WIN
- `SPC` : (Space)   la touche Espace
- `buffer` : Un heu [buffer](https://www.gnu.org/software/emacs/manual/html_node/emacs/Buffers.html)

### Eu'l néon bleu, y 'claire la route
- Je sauve mes fichiers avec `M-S`, c'est un mouvement beaucoup plus naturel pour moi
- Je les ouvre avec `C-o` parce que ma mémoire musculaire me l'a ordonné (fichiers récents `M-o`)
- Je sélectionne tout avec `C-a` pasque bon, quoi
- Je (dé)commente avec `M-d` (sélection ou pas sélection) pour les même raisons
- Je vérifie l'orthographe d'un buffer avec `F7`, parce que c'est le raccourci standard
- J'ai remappé `isearch-forward` (voir "Recherche") sur `C-f` (mais j'ai gardé `C-s`, les deux combinaisons font la même chose)
- j'ai écrit une fonction qui sélectionne *n'importe quoi situé entre deux signes identiques* (et je m'en sers tout le temps)
- Quand je `M-i` le mini-buffer m'affiche toutes les expressions (fonctions, classes, etc.) définies dans le fichier que je peux afficher à la volée **dans n'importe quel langage** (cette fonction n'est pas de moi, c'est [un chef-d'oeuvre](http://www.emacswiki.org/emacs/ImenuMode#toc11))
- Je bouge la ligne courante avec `M-up` `M-down`, je peux chercher la sélection sur le web d'un coup de `C-c g`
- Quand je tape `C-(` ça fait `()` si rien n'est sélectionné, ou `(texte sélectionné)`, et ce avec *tous les doubles signes courants*. Si, si.
- Mon historique d'undo est persistente entre les sessions (à peu près tout est persistant en fait)
- Quand j'appuie sur `C-M-SPC` (tu es assis là, hein ?) à répétition, Emacs me sélectionne *la prochaine expression* (**dans n'impor** enfin toi-même tu sais). Donc tu te mets devant un `for (condition) { ...` et il te sélectionne toute la boucle jusqu'à la dernière accolade (à la fin il émet une erreur, qui annonce la fin de l'expression, et indique que ce que tu voulais sélectionner, l'est) ce qui permet de sélectionner comme qui rigole des if / else de quatre pages écrans (ça (`(mark-sexp &optional ARG ALLOW-EXTEND)`) c'est built-in)
- Bien entendu cette config est synchro sur toutes mes machines.

### Mark-ring
Quand tu te déplaces dans le fichier de façon rapide, genre `C-end` pour aller voir un truc à la fin du fichier, comment tu reviens "là où tu étais" ?
Emacs maintient un "mark ring" qui contient toutes les positions de départ de ce type, l'endroit où tu as commencé une recherche ou un mouvement, et fais une marque à chaque fois qu'un de ces déplacements se produit.
`C-u C-SPC` permet de remonter dans le mark-ring (je l'ai mappé sur `s-left`). Ça n'a l'air de rien, c'est *extrêmement* puissant (et un [peu mystique](https://www.gnu.org/software/emacs/manual/html_node/emacs/Mark-Ring.html), faut bien le dire).

### Kill-ring
Quand tu "killes" un mot, une ligne, avec `C-w` ou `M-backspace` par exemple, l'objet passe dans le "kill-ring", qui est accessible avec `M-y` (apres un `C-y` seulement) ce qui permet de jongler avec le presse-papiers pour permuter des valeurs en sifflotant.
Note que **ça marche** aussi **dans ton shell**, ce qui rend l'étude de cette killer-feature indispensable.

### Rectangles
`C-x r k` Pour killer un rectangle, `C-x r t` pour insérer une string.

### Macros
Pour démarrer l'enregistrement, `C-x (` et pour l'arrêter, `C-x )`.
Pour exécuter, `C-x e` (je l'ai mappé sur `C-kp-0`). Ça juste marche.

### Recherche
Quand tu est devant un mot, et que tu veux chercher ce mot (ça t'arrive tout le temps, cherche pas) tu appuies sur C-s ([chez moi](https://github.com/xaccrocheur/kituu/blob/master/.emacs) `C-f` marche aussi) tu passes alors en mode `incremental-search-forward` (`C-r` pour basculer en backwards anytime) maintenant sans lacher control, appuie sur `w` : La sélection commence à "manger" le mot, puis les autres à mesure que tu rappuies sur `w`. Pendant ce temps, toutes les occurrences de cette sélection sont colorées dans le buffer. Si tu rappuies alors sur `s`, tu cycles toutes ces occurrences. That's how we search in the Shire.

### Tramp (Transparent Remote (file) Access, Multiple Protocol)
Ouvrir un fichier à distance (chez moi `C-o`, remember)
`C-x C-f /ssh:user@machine:~/fichier.ext`

Ouvrir un fichier as root
`C-x C-f /sudo:root@localhost:/etc/fichier.ext`

### Server
Pour n'avoir qu'un seul Emacs ouvert et y charger tous les fichiers voulus à la volée, enregistrer le script shell suivant sous `~/bin/e` :

\#!/bin/sh

```sh
if [ "$(pidof emacs)" ] ; then
    emacsclient "$@" &
else
    emacs -mm "$@" &
fi
```

Et ouvrir les fichier comme ça

`e ~/.zshrc &`

(astuce : Lancer le 1er `~/bin/e` de la journée avec le shell graphique, genre `Ctrl-F2 ~/bin/e`, gnomedo ou équivalent, pour éviter qu'un crash du shell - shit happens - ne kille Emacs, ce qui est criminel)

Packages
-----------
`M-x package-list-packages`
Il faut avoir des dépôts définis, voir [mon .emacs](https://github.com/xaccrocheur/kituu/blob/master/.emacs) à la section "packages"

### Undo-tree
L'undo d'Emacs est très puissant, si tant qu'il confuse les nouveaux arrivants qui peine à comprendre comment marche le redo. Ouvre la fenêtre, respire un peu. Le redo est un undo de l'undo.

```
           Undo/Redo classique             Emacs' undo

              o                                o
              |                                |
              |                                |
              o                                o  o
              .\                               |  |\
              . \                              |  | \
              .  x  (new edit)                 o  o  |
  (discarded  .                                | /   |
    branch)   .                                |/    |
              .                                o     |
                                                     |
                                                     |
                                                     x  (new edit)
```

Alors certes, ça permet de ne jamais perdre un edit, mais l'undo-ring peut devenir long à naviguer, surtout si on n'a pas compris. Ça peut devenir plus dur que de convertir des °F en °C pendant que ta maison brûle :

```sh
     (trying to get   o                          x  (finally got there!)
      to this state)  |                          |
                      |                          |
                      o  o     o     o     o     o
                      |  |\    |\    |\    |\    |
                      |  | \   | \   | \   | \   |
                      o  o  |  |  o  o  o  |  o  o
                      | /   |  |  | /   |  |  | /
                      |/    |  |  |/    |  |  |/
     (already undid   o     |  |  o<.   |  |  o
      to this state)        | /     :   | /
                            |/      :   |/
                            o       :   o
                                    :
                            (got this far, but
                             broke the undo chain)
```

Undo-tree permet non seulement un contrôle plus simple en définissant un redo, mais en gardant l'arbre, qui est visualisable et cliquable. Il affiche aussi un diff en temps réel, et les timestamps avec `t` ; c'est un truc de dingue.


```
                               o
                           ____|______
                          /           \
                         o             o
                     ____|__         __|
                    /    |  \       /   \
                   o     o   o     o     x
                   |               |
                  / \             / \
                 o   o           o   o
```

Chez moi ces commandes sont mappées `C-z`, `C-S-z`, et `C-x u` pour l'undo-tree. qui sauve ma journée assez régulièrement, merci [undo-tree](http://www.dr-qubit.org/emacs.php#undo-tree-docs).

### Magit
Magit permet une utilisation en profondeur des fonctions de git : Il permet de voir ce qui est staged, commited, (un)pushed et de gérer ces objets de façon très fine, en stageant à la volée `s` seulement les fichiers désirés et en ne pushant `P P` que les commits `c` voulus, et ce n'est que la première ligne de la réponse à la question "Magit?"

### Kituu
Pour tester mon [environnement](https://github.com/xaccrocheur/kituu), entrer ces deux commandes :

- `cp -R ~/.emacs.d/ ~/.emacs.d.tmp/`
- `cd & wget https://raw.github.com/xaccrocheur/kituu/master/.emacs`

(ou carrément `git clone https://github.com/xaccrocheur/kituu.git .kituu && .kituu/scripts/kituu-setup.sh` mais là c'est *tout* mon environnement que tu clones, alors [RTFM](https://github.com/xaccrocheur/kituu/blob/master/README.org))

Une fois Emacs lancé, `px-help-emacs` rappelle mes raccourcis spécifiques (pas tant que ça) et d'autres trucs moins évidents que j'ai eu du mal à retenir.

Tu es dans le "Scratch buffer" ; entre `(animate-string "Welcome to Emacs, little mussel!" 10 10)` reste positionné après la dernière parenthèse, et entre `C-x C-e` (eval-last-sexp) ; Bonne exploration :)

Documentation
-----------
Pour savoir ce que fait une combinaison de touches `C-h k "combinaison"` ; pour avoir la liste de tous les raccourcis clavier (bindings) `C-h b`. Toutes les docs sont dans Emacs lui-même, `C-h i`. Des liens mênent vers les very définitions des fonctions et variables directement dans les fichiers source. Oui, pour les fonctions C aussi, je sais même pas comment il fait ça.
Pour lancer le tutorial, `C-h t`. Pour le psy, `M-x doctor` (les enfants adorent, ma fille a appris l'anglais avec).

La mailing-liste [help-gnu-emacs](https://lists.gnu.org/mailman/listinfo/help-gnu-emacs) est plein de hippies géniaux et de professeurs éminents qui font des blagues rigolotes.

Épilogue
-----------
Ce petit journal est juste un bout minuscule de ce qu'une moule bien réveillée peut faire avec Emacs. Oui je sais, c'est [mystérieux](http://common-lisp.net/project/slime/). Be [afraid](http://vimeo.com/47578617). Be [very](http://emacsrocks.com/) afraid.

Emacs a vaincu ma résistance. Comme la musique, il a déformé mon corps. Emacs a ruiné ma vie. G-Gollum ! ;)
