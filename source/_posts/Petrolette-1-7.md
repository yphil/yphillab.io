---
title: Pétrolette v1.7 is out
date: 2023-05-16 10:13:05
tags:
- pétrolette
- JS
---

This is quite a substantial release, I've been thinking about [those new social media feeds](/blog/2023/05/16/feeds_galore/) lately, also a lot has changed on the web, mostly for the best (more corporate big-tech censorship, leading to more indy sources) but not always (like the sudden death of the Dilbert RSS feed :/) ; This release has been almost ready for *months* on my local machine but I've been so snowed under lately that I guess I forgot to finish, test and push it :|

[The public instance](https://petrolette.space) is up-to-date, get [yours](https://framagit.org/yphil/petrolette) while it's hot 😎

![This icon pertains to the user account, not the main website](/images/v1.7.jpg)


The "official" changelog is [here](https://framagit.org/yphil/petrolette/-/blob/master/CHANGELOG.md) and may very well change, but there you go so far:

## Changelog

### v1.7.0
- Pétrolette now gets the specific icon from the feed, not just the one of the main website ; This is particularly useful for social media feeds, where the icon is different for each feed / user. If you use such "main site and not specific user's icon" feeds, read on.
- New "Reset icon" button to request (and thus cache) a newer icon than the one referenced in the cache
- Links in the feed item are now clickable (and abbreviated)
- Small script to test each feed URL for any errors (like the feed simply doesn't exist anymore) in one command

### Bugfixes
- Don't remember, so probably not a lot
- Removed some debug symbols
