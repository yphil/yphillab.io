---
title: Betatest harmonySEQ - Réveille le cyberponk 🇫🇷
date: 2013-07-31 10:13:05
tags: audio
---

Ce WEnd, préviens les voisins, met un postit dans l'ascenseur "Ce WEnd, un réveil de cyberponk est organisé ; nous nous excusons par avance de l'avalanche de décibels qui va s'abattre sur ce bâtiment et vous invitons à danser en rond ou à fuir en bon ordre".

![Bien sur j'ai perdu les fichier d'origine](https://github.com/rafalcieslak/harmonySEQ/raw/master/images/screenshot2.png?raw=true)

<!-- toc -->

Pré-requis
-----------

Il te faut :

- Un ordinateur connecté à l'internet muni d'une carte son branchée sur des enceintes stereo
- Deux packs de bière, la moins chère (important)
- Une guitare japonaise à 100€ (facultatif)

Boire les 6 premières bières.

[HarmonySEQ](http://harmonyseq.wordpress.com/about/) est (burp) tout simplement une [ré-vo-lu-tion](http://www.louigiverona.ru/?page=projects&s=writings&t=linux&a=linux_harmonyseq) dans le monde de l'audio tout court, pas seulement sous Linux. Il n'existe à ce jour [aucune documentation](http://harmonyseq.wordpress.com/faq/), tu y crois à ça ? Non, bien sur. Remember where you about read it first ;)

Colle-toi au bouzin, et entre les commandes suivantes :

``` sh
sudo add-apt-repository ppa:rafalcieslak256/harmonyseq
sudo apt update
sudo apt install qjackctl harmonyseq hydrogen qsynth fluid-soundfont-gm
```

Lance qjackctl ; Clique sur l'icône de configuration et dans "input device" et "output device" (en bas à droite) sélectionne la bonne carte-son. Ne touche à rien d'autre avec tes doigts sales ; ferme la fenêtre de prefs, et accepte de relancer le serveur.

Lance le bazar en buvant une bière entre chaque :

``` shell
harmonySEQ
hydrogen
qsynth
```

Dans qjackctl, clique sur le bouton "Show the actual connections patchbay window".

Dans l'onglet "Audio", connecter

- La sortie d'hydrogen à l'entrée "system"
- La sortie de qsynth à l'entrée "system"

Dans l'onglet "ALSA", connecter

- La sortie d'harmonySEQ à l'entrée "hydrogen"
- La sortie d'harmonySEQ à l'entrée "FLUID Synth"

Dans Qsynth, clique sur "setup", onglet "SoudFonts", clic sur "open" et charge "/usr/share/sounds/sf2/FluidR3_GM.sf2".

Dans hydrogen, menu tools => preferences, onglet "Midi system" :
- Input : harmonySEQ output
- Channel : 16

New note sequencer
-----------

Dans la barre d'outils d'harmonySEQ, clique sur "add new note sequencer".
Dans le champ "MIDI channel" entre 16.
Remplis la dernière des 6 lignes de la pattern de carrés bleus (clic clic hur hur) nomme ce sequencer "HH" en cliquant sur le nom "seq 01" à gauche (Clic gauche ajout, clic droit suppression).

Events, actions
-----------

Dans la partie droite de la fenêtre d'harmonySEQ, clique sur le bouton "new event". Clique sur "Capture" et enfonce joyeusement la
touche F1. Puis directement après clique sur "New action" et sélectionne l'action "Sequencer/Swith On-Off" et dans le menu "Sequencer", "HH".

Puis définis un nouvel event "p" avec l'action "Play/Pause" et l'option "toggle".

Frappe la touche p, puis F1. Bien fait pour elles.

Dans la dernière des 6 lignes de la pattern, met le curseur dans le champ numérique juste à gauche de la ligne, et descend avec la touche down ou la molette de la souris jusqu'aux valeurs -10 à -20 (YMMV) pour nous sortir un bon son d'HH (Hi Hat) qui va bien. On peut mettre des effets dans hydrogen, 4 pour tout le monde, tu vois dans le mixer les 4 rotatifs au-dessus des solo/mute de chanque tranche ? Ce sont les 4 départs des effets 4 en question, mais je digresse, burp.

En principe à ce stade, on entend "tss tss tss tss tss tss" en boucle, voir POUM TCHAK si tu t'es fendu d'un kick et d'une snare (you headbanger you). appuie sur p pour arrêter ce wakarme et bois une autre bière.

Répéte le processus de création de sequencer/piste mais cette fois appelle-là "bass", MIDI channel 0 et trouve-nous un bon son de basse qui va bien à l'aide la même méthode que t'haleure. Affecte-lui la touche F2. Si tu entends qsynth sur la piste de drums, retourne dans qsynth => channels => 16, right click, "unset".

Même démarche pour la piste "chordz", et cette fois entre un accord : Superpose trois barre sur trois ou quatre carrés horiz.

Et là attention ça va aller assez vite et devenir violemment cool :

- Crée un Event "c" et une action :
- sélectionne "Sequencer/Set chord" Sequencer : "bass" type "custom"
- Coche "do not change chord's octave"
- Rajoute une action "Sequencer/Set chord" Sequencer : "chords" type "custom"
- Maintenant crée un event "f" et la même action mais dans le menu qui dit "C", sélectionne "C#"
- Rajoute la même action mais Sequencer : "chords"

Appuie sur p, puis enclenche tes sequencers avec les FKeys. Puis bois une grande rasade de binouze et appuie sur "f". Hein ? Hein ? Ouais. HarmonySEQ connait les modes et permet de se promener chromatiquement dans les patterns, entre autres évènements genre changement d'octave. Et là je pose la question : How wickedly cool is that?

### Multi-pattern

Dans chaque sequencer à droite du pianoroll se trouve la fenêtre de clonage de pattern. Après, tu crées une action "Sequencer/Set active pattern" et roule ma poule. De la bombasse chtedis.

Enregistrement MIDI
-----------

À la fin de la nuit, pour envoyer une belle partition à Mamie avec en CC l'académie Charles Cros, on peut enregistrer le fichier MIDI à l'ancienne :

``` shell
sudo apt install alsa-utils musescore
aconnect -il
(...)
client 134: 'harmonySEQ' [type=user]
0 'harmonySEQ output'
Connecting To: 133:0, 129:0[real:1], 135:1
(...)
```

Repère le numéro de la sortie MIDI d'harmonySEQ (ici 134) puis ;

``` shell
sudo arecordmidi --port=134 ~/tmp/reveille_le_ponk.mid
```

Enregistrement Audio
-----------

``` shell
sudo aptitude install jack-mixer audacity
```

Lance les deux, dans jack_mixer crée deux "input channel" et dans qjackctl, connecte-les à qsynth et hydrogen. Crée ensuite (dans jack-mixer) un "out channel" et dans audacity, preferences => device => interface => Host "Jack".
Démarre l’enregistrement dans Audacity.

Puis dans harmonySEQ, joue ta compo, avec tes changements de pattern, d'accord, et tout l'bazar. Ctrl-c pour arreter arecordmidi.

Pour écouter le Midifile (jack tourne, hein ? Vérifie) :

``` shell
timidity -Oj ~/tmp/reveille_le_ponk.mid
```

Édition de la partition
-----------

Et enfin,

``` shell
musescore ~/tmp/reveille_le_ponk.mid&
```

Tu peux mettre de l'ordre dans ta partition en "create instruments" pour organiser violons, guitares, perceuse, etc.
Ctrl-p, Bzzz-clac et tu peux le montrer à mamie qui va l'encadrer, c'est sur.

Maintenant, café.
-----------

En fait, l'idée, après, c'est plutôt de faire quelque chose de sensé du midifile, dans un sequencer "normal" genre qtractor. [HarmonySEQ](http://harmonyseq.wordpress.com/) est un fantastique outil de composition, il demande peu de ressources, l'idéal pour occuper une heure de train en révisant la gamme pentatonique en mode mineur ; jackd est grand. Kipitril.
