---
title: MobilOhm Beta campaign start
date:  2023-09-04
tags:
- mobile
- JS
---

## 3 apps

Actually, four, but [Pétrolette](https://petrolette.space/) is a bit apart in that it's not a PWA, being a online-only app, but I digress. This is the start of the [MobilOhm](https://mobilohm.gitlab.io/) beta campaing.

![MobilOhm website](/images/mobilohm-website.jpg "MobilOhm website")

By the way do you realize that with self-hosted apps (ie built and served on the very repository infrastructure, with CI/CD jobs details available) you get the rare **garantee** that the code you use is the *same* that you see on said repo? This is like a whole new meaning for [WYSIWIG](https://en.wikipedia.org/wiki/WYSIWYG) 🤓

Quite a far cry from the highly [convoluted](https://yphil.gitlab.io/2023/07/16/dolores-en/) APK / ADB bundle build / release cycle... Kinda makes you wonder what to do of messages like [this one](https://forum.f-droid.org/t/progressive-web-apps/1691/4?u=yphil)..?

All the apps are in beta testing now ; use them with only test data, on as many machine and architecture types as possible. There are no plans to modify the data structures anyway. If a potentially breaking change happens it would be reflected in the minor version increment. Please report any issues, see below on directly within the respective app. I'm going to give this beta test campaign a couple of weeks before releasing them in the wild.

### Principles

- Auto-hosted ;
- Syncs the main data locally, then in the cloud ;
- Saves the app preferences in [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) (to allow different displays on each device) ;
- Share a common app - data - navigation - structure / paradigm, to the point where you can almost call it an app framework ;
- GPL3.

### Technologies

- [Progressive Web Apps](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps)
- [Bulma CSS](https://bulma.io/)
- [Vanilla JS](https://developer.mozilla.org/en-US/docs/Web/JavaScript/JavaScript_technologies_overview)
- [Remote Storage](https://remotestoragejs.readthedocs.io)

## My job

Was to port those 3 apps from Android [APK](https://yphil.gitlab.io/2023/07/16/dolores-en/) to [PWA](https://yphil.gitlab.io/2023/08/11/pwa-ftw/) ; Note that I did it all during the summer holidays ;)

- From Java soup to pure HTML
- From DBIndex to offline-first remote synchro
- From tedious binary blob to feather-like web poetry

## Your job

Is to test one or more of them and report to the mothership using the "Report issues" buttons in the About page of each app.

| Name       | Install / use                                 | Report issue                                                                              |
|------------|-----------------------------------------------|-------------------------------------------------------------------------------------------|
| Dolores    | [Install](https://dolores-workout.gitlab.io/) | [Report issue](https://gitlab.com/dolores-workout/dolores-workout.gitlab.io/-/issues/new) |
| Todonna    | [Install](https://todonna.gitlab.io/)         | [Report issue](https://gitlab.com/todonna/todonna.gitlab.io/-/issues/new)                 |
| Papiers    | [Install](https://papiers.gitlab.io/)         | [Report issue](https://gitlab.com/papiers/papiers.gitlab.io/-/issues)                     |
| Pétrolette | [Use](https://petrolette.space/)              | [Report issue](https://framagit.org/yphil/petrolette/-/issues/new)                        |

"Go ahead and test".

## And now?

It's 23:32 here, I'm gonna turn in.
