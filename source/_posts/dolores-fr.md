---
title: Du Web au Mobile avec Capacitor 🇫🇷
date: 2023-07-16 12:10:27
tags:
- mobile
- JVM
- JS
---

## Je suis développeur mobile ! (on me dit jamais rien)

- Code: https://framagit.org/yphil/dolores

Le paysage du développement mobile a pas mal bougé au fil des dernières années. Autrefois, les développeurs mobiles étaient contraints d'utiliser des langages spécifiques : Java pour Android, Objective-C et Swift pour iOS 😱 Mais depuis quelques années une révolution est en marche ; les développeurs web "full stack" peuvent désormais créer des applications mobiles complètes en utilisant leurs **décennies** d'expérience en HTML, JavaScript et CSS.

![Android](https://developer.android.com/static/images/brand/Android_Robot.png "C'est le logo officiel")

## TL;DR: Où c'est ?
Dans ce post, on va explorer comment un développeur web peut devenir mobile en utilisant [Capacitor](https://capacitorjs.com/), un framework qui fait le lien entre ton code et ton phone et permet de construire des apps mobiles complètes en utilisant HTML, JavaScript et CSS, sans avoir à dealer avec Java directement, quelle horreur. J'ai fait [Dolores](https://framagit.org/yphil/dolores) parce que j'en ai besoin, mais c'est également un exemple concret d'app mobile multiplateforme créée avec Capacitor.

![Dark Dolores ; La base indexDb a marché direct, l'hallu 🙂](https://framagit.org/yphil/assets/-/raw/master/img/Dolores/screenshot-01.png "Dark Dolores")

![Light Dolores](https://framagit.org/yphil/assets/-/raw/master/img/Dolores/screenshot-02.png "Light Dolores")

## De l'aventure des plateformes alternatives
Dans le passé, certains dinausores comme Palm Pilot, ou plus tard Nokia Maemo ont tenté de bousculer le développement d'applications mobiles natives en utilisant des langages tels que JavaScript et HTML. Malheureusement, elles ont fini par disparaître en raison de "divers obstacles et d'une adoption limitée sur le marché", ce qui est une façon polie de décrire ce qui s'est réellement passé - j'étais *là* - car c'est pas (plus) le sujet.

## Donne-leur ce qu'ils méritent, retire l'OOP de ton CV
Traditionnellement, on associait le développement mobile à la programmation orientée objet (POO) et à la croyance selon laquelle "on ne peut pas créer une interface graphique sans la POO". Sans vraiment réaliser qu'on fait de l'objet en JS nativement, en JS même les fonctions sont des objets. Avec les frameworks et outils modernes, les développeurs web peuvent maintenant créer des interfaces utilisateur aux petits oignons sans écrire une seule ligne de Java. Oui parce que le problème c'est pas les objets, l'héritage, les classes ou même et surtout pas la JVM, le problème c'est Java.

## Capacitor : Faire le lien
Une avancée significative dans ce domaine est Capacitor, qui permet aux développeurs de créer des applications mobiles multiplateformes en utilisant les technologies web. Capacitor est construit sur Apache Cordova (anciennement connu sous le nom de PhoneGap) et est [maintenant maintenu par l'équipe Ionic](https://ionic.io/resources/articles/what-is-apache-cordova). Il sert de pont entre les technologies web et les fonctionnalités natives des appareils mobiles.

![Capacitor](/images/capacitor.webp "Capacitor")

### Workflow : De la web à la mobile app avec Capacitor
Le processus de création / dev d'applis mobiles avec Capacitor suit un flux de travail simple qui met à profit tes compétences existantes en développement web :

1. Code ton application en utilisant HTML, JavaScript et CSS, tout comme tu créerais une application web;
2. Installe le SDK Android à l'aide d'[Android Studio](https://developer.android.com/studio), et adapte ton environnement;
2. Installe Capacitor, ajoute les plateformes, synchronise ton code - au 1er lancement, cela va juste le copier ; oui tout, HTML, CSS, JS, images, je crois qu'il récupère même le code de tes éventuels CDNs pour l'embarquer;
2. Utilise [les API de Capacitor](https://capacitorjs.com/docs/apis) pour accéder aux fonctionnalités natives des machines, comme l'appareil photo, la géolocalisation ou les notifications;
3. Adapte (ou pas) l'apparence et le comportement de ton application en utilisant des frameworks standard comme [Bulma](https://bulma.io/). #tip Crée-toi une fonte custom avec les icones qui vont bien à l'aide de [Fontello](https://fontello.com/);
4. Teste ton app en utilisant le serveur de développement de Capacitor ou en l'exécutant directement sur des appareils physiques ou des émulateurs. J'ai pas réussi à faire tourner celui d'Android Studio, oh well, quand on développe une web app on n'a pas besoin d'émulateur anyway 😎
5. Déploie ton application sur les app stores et / ou directement sous forme de téléchargement de l'APK bundle, puis distribue-la aussi sous forme d'application web.

### Environnement

``` shell
export ANDROID_HOME=~/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/platform-tools/
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/latest/bin/
export PATH=$PATH:$ANDROID_HOME/emulator/

export JAVA_HOME=~/bin/android-studio/jbr
export CAPACITOR_ANDROID_STUDIO_PATH=~/bin/android-studio/bin/studio.sh
```
### Création / import du projet

``` shell
cd www
npm i @capacitor/core
npm i -D @capacitor/cli
npx cap init
npm i @capacitor/android @capacitor/ios
npx cap add android
npx cap sync
```

### Build!
En ligne de commande:
``` shell
npx cap build android
```

Dans Android Studio:
``` shell
npx cap open android # d'où $CAPACITOR_ANDROID_STUDIO_PATH
```
- Menu "Build" / "Generate Signed Bundle / APK";
- Crée une clef de signature du paquet
- Choisis "release"
- l'APK installer est à `android/MonProjet/release/MonProjet-release.apk`

### Desktop compatible
Et bien sur ton appli reste utilisable sur le desktop:
``` shell
xdg-open www/index.html
```
## Ça sent le café
Les jours où le développement mobile demandait du sang, de la sueur et des larmes sont loin maintenant ; grâce à l'émergence d'outils comme Capacitor, on (nous, les "développeurs *web*", tu vois ce que je veux dire) peut désormais s'y lancer en toute confiance. Si comme moi tu codes des applis web de plus en plus *puissantes* depuis des **décennies**, tu as une base solide ; embrasse ce nouveau pouvoir. Tu *es* un développeur mobile 😎
