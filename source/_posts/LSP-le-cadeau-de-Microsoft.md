---
title: LSP, le cadeau de Microsoft 🇫🇷
date: 2021-05-19 19:17:58
tags:
- lisp
---
[LSP](https://en.wikipedia.org/wiki/Language_Server_Protocol) (Language Server Protocol) est un protocole ouvert basé sur [JSON-RPC](https://en.wikipedia.org/wiki/JSON-RPC) pour la communication entre le logiciel éditeur / [IDE](https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement) et un serveur qui lui fournit les informations sur un langage spécifique.

Le but du protocole est de faciliter l’implémentation du support de n’importe quel langage dans un éditeur donné.

![LSP / Emacs (Serveur: ts-ls, Client:js2-mode)](https://i.postimg.cc/76WQBbCT/lsp-mode.png)

# Moby Dick

L’intégration du support des langages de programmation dans les éditeurs de texte est une baleine blanche qui fait surface régulièrement et dévaste tout sur son passage. Rien que sous Emacs, les tentatives de faire de l’éditeur un IDE rempliraient une collection de livres de bonne taille ; nous y reviendrons.

# Qu’est-ce qu’un IDE ?

Très simplement, un IDE est un éditeur de code qui *sait* dans quel langage tu programmes.

Un peu comme Clippy, le trombone de Word des 90s, qui lorsque l’utilisateur tapait « Adieu monde cruel, je veux en fin… », semblait comprendre ce qu’il écrivait et lui proposait de l’aider en lui posant des questions pertinentes, telles que son choix de modus opérandi ou ses raisons profondes afin de rédiger avec lui une lettre de suicide percutante et bien balancée, LSP _reconnait_ du code en Scala, JS, C, Typescript, Java, Python ou [autres](https://emacs-lsp.github.io/lsp-mode/page/languages/), et propose son expertise.

# Fonctionnalités

Les implémentations varient selon les plateformes (combinaison serveur / client) mais la liste des fonctionnalités est à peu près la même à chaque fois.

## Coloration syntaxique

Selon l’implémentation, c’est soit l’éditeur qui s’en occupe, soit LSP ; dans Emacs sous `js2-mode` par exemple, c’est ce dernier (le _mode_) qui est le _client_ LSP.

Des fonctions “toggle” permettent de permuter l’affichage de tout : commentaires, fonctions, etc.

## Validation / Nettoyage (linter)

Les variables / constantes inutilisées ou au contraire, non déclarées, les erreurs de type, de portée, ou tout simplement de syntaxe, tous ces détails sont signalés.

Bien sur, la façon dont tout ça apparait (en popup / overlay, dans le minibuffer, dans un buffer séparé, discrètement dans un menu, etc.) est paramétrable en fonction des implémentations côté client.

## Ré-usinage (re-factoring)

Là, on commence à vraiment parler d’IDE : renommage d’objets (variables, fonctions / méthodes, etc.) au travers de tout le projet, **suggestion d’éditions** (ce module CommonJS pourrait passer en ES6, cette fonction anonyme pourrait être en « fat arrow », cet argument non utilisé devrait être préfixé d’un `_`, cette classe pourrait être déplacée dans un fichier séparé (si !) etc.) je peux le faire pour toi si tu veux, appuie ici (la petite ampoule bleue dans la capture, ou `C-c l a a`) **boum**.

Des règles / conventions de formatage peuvent être définies, et surtout appliquées par toute une équipe, **indépendamment de l’éditeur avec lequel chacun préfère travailler**, simplement en partageant la config LSP.

## Documentation

Le curseur positionné sur un objet quelconque, le serveur indique tout ce qu’il en sait, puisqu’il embarque la doc _exhaustive_ du langage et des libs utilisées, et sait également lire les _docstrings_ des objets spécifiques au projet.

## Définitions et références des objets

Où cette constante est-elle utilisée ? Où cette variable a-t-elle été déclarée ? À quelle classe appartient cette méthode, et où est donc Ornicar ? LSP rend très facile le déplacement dans le projet en affichant à la demande la liste de tous les types d’objets d’un fichier / projet donné.

![LSP / Emacs (Serveur: metals, Client:scala-mode)](https://mamot.fr/system/media_attachments/files/109/080/771/815/222/737/original/2fb81183ec99801c.png)


## Introspection

Quel est le type de cet objet ? Exactement comme l’inspecteur des dev tools de Mozilla / Chrome, il est possible d’en apprendre beaucoup sur un objet juste en le sélectionnant.

## Débogage

NodeJS n’avait pas réellement de débugger, c’est maintenant chose faite. Breakpoints, traces, pile d’appel (call stack / trace) locals, tout y est, et même un REPL (Read-eval-print loop) pour accéder aux données du programme en temps réel.

J’ai voulu ré-écrire ce dernier paragraphe pour lui donner tout le _gravitas_ qu’il mérite, mais je pense que le fond du propos est suffisamment **phénoménal** pour se suffire à lui-même :)

# Les hippies de chez 'Crosoft

Le concept du protocole LSP vient d’un de ces nombreux éditeurs / IDE qui ont voulu s’attaquer à la baleine, en l’occurrence l’ancêtre de Visual Studio Code, orienté C#, [Omnisharp](https://docs.microsoft.com/en-us/visualstudio/extensibility/language-server-protocol?view=vs-2019#how-work-on-the-lsp-started) ; au moment de développer les interfaces d’introspection de VSCode, l’équipe a voulu réutiliser ce concept de « serveur de langage » et LSP est né.

Et bien sur il est publié sous licence libre, vu que c’est l’été de l’amour tous nus dans les blés à Redmond, comme chacun a remarqué depuis un moment déjà :)

Aujourd’hui, il est certes intégré à VSCode, mais donc également disponible pour tout le monde, qui (le monde) n’a pas été long à se saisir de l’opportunité de se débarrasser de cette logique extrêmement contraignante, pour la déléguer à un serveur externe et se concentrer uniquement sur les capacités d’édition à proprement parler ; c’est vrai quoi, chacun son boulot, et un éditeur, pour faire par exemple du C++ n’a pas à embarquer toute la documentation (à jour) du langage dans le logiciel lui-même.

Ouf.

----
- [Documentation (GitHub Microsoft)](https://microsoft.github.io/language-server-protocol/)
- [Présentation vidéo de lsp-mode par System Crafters ](https://www.youtube.com/watch?v=E-NAM9U5JYE)
- [Présentation vidéo de lsp-dab (le débogueur) par System Crafters](https://www.youtube.com/watch?v=0bilcQVSlbM)
- [LSP / Emacs ](https://emacs-lsp.github.io/)
- [Ma configuration](https://framagit.org/yphil/dotfiles/-/blob/master/.emacs)
- [LSP / Vim](https://bluz71.github.io/2019/10/16/lsp-in-vim-with-the-lsc-plugin.html)
- [Langserver.org: a community-driven source of knowledge for Language Server Protocol implementations](https://langserver.org/)
