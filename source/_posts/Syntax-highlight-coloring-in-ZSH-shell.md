---
title: Syntax highlight / coloring in ZSH shell
date: 2022-10-18 12:54:51
tags: 
- shell
---

Here is another thing that should be default, and as a matter of fact is [for me](https://framagit.org/yphil/dotfiles), since like a good five years:

## Syntax coloring in the shell

And we're not talking about the prompt's color, of even the command outputs, I mean, this is generally taken care of, here is the output of `ls -la` in a vanilla Ubuntu:

![Linux ls -la output exerpt (with user & group cropped out)](/images/zsh-ls.png)

No, this is about ***what you're actually typing in your shell in real time*** :

![Syntax highlight / coloring in ZSH shell](/images/zsh-syntax-coloring.png)

In this example (`tmux` in `gnome-terminal`) we have a pretty nice demo : For starters commands are validated in real time : if you enter a command name that is not in the path it is colored in red, valid commands are in green, **all** commands and aliases.

But the most powerful is the coloring of script commands, of course. Reminder : The scripting languages of Bash and Zsh are different but compatible, in fact personally although I've been a Zsh user for well over 15 years now, all my shell scripts start with `#!/usr/bin/env bash` and typically, the `for` loop above is in Bash (in Zsh it's shorter, no need for the `do` / `done`) 🤓

## To install this indispensable thing

``` shell
cd ~/src
git clone https://github.com/zsh-users/zsh-syntax-highlighting
```

### in `.zshrc`

``` shell ~/.zshrc
[[ -a ~/src/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]] && . ~/src/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
``` 

This is testing for the presence of the file, and is sourcing it if found ; The `.` means `source`. But you knew that already.
