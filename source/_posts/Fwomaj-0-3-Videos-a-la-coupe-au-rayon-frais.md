---
title: Fwomaj 0.3 - Vidéos à la coupe au rayon frais 🇫🇷
date: 2016-07-12 16:55:25
tags:
- python
- audio
- video
---

Fwomaj est un lecteur multimédia qui permet la sélection rapide de fichiers vidéo au retour d'une séance de tournage (on dit "dérusher") pour un montage ultérieur. Fwomaj est sous licence GPL 2.

![Fwomaj 0.3](https://bitbucket.org/yphil/fwomaj/raw/master/screenshot.jpg)

En premier lieu, il affiche la forme d'onde de la piste audio du fichier, afin de savoir tout de suite s'il s'y trouve du son, et à quel endroit.

Il permet également le transcodage de la vidéo en [H264](https://fr.wikipedia.org/wiki/H.264), et sa sauvegarde dans les trois containers courants: [Matroska](https://fr.wikipedia.org/wiki/Matroska) (.mkv) [MPEG-4](https://fr.wikipedia.org/wiki/MPEG-4) (.mp4) et [AVI](https://fr.wikipedia.org/wiki/Audio_Video_Interleave) (.avi) que l'utilisateur choisit à l'aide d'un menu.

Et enfin, il permet de couper le début et la fin de la vidéo à l'aide de contrôles visuels.

En bonus, il fournit également une fenêtre d'information qui liste les caractéristiques multimedia du fichier [de façon exhaustive](#Mediainfo).

![Fwomaj ICON](https://linuxfr.org/images/historique/images_perdues/fwomaj-0-3-videos-a-la-coupe-au-rayon-frais-Ykmx3zdOIrGX.png)

Code
-----------
Fwomaj est en [Python 3](https://docs.python.org/3/), l'interface est en [Gtk 3](https://lazka.github.io/pgi-docs/Gtk-3.0/index.html). Le moteur vidéo (et audio) est [GStreamer 1.0](https://developer.gnome.org/gstreamer/1.0/).

### Dépendances
- [Python](https://www.python.org/)&nbsp;;
- [GStreamer](https://developer.gnome.org/gstreamer/stable/)&nbsp;;
- [FFmpeg](https://ffmpeg.org/)&nbsp;;
- [Mediainfo](https://mediaarea.net/).

### Son & Lumières
C'est bien sûr [FFmpeg](https://ffmpeg.org/documentation.html) qui fait le gros du travail : c'est lui qui génère l'image de la forme d'onde, lui encore qui découpe le fichier de `temps` à `temps+n`, et lui enfin qui encode tout ça, à son rythme de supertanker.

Au moment où l'utilisateur appuie sur `Encode` on récupère les valeurs des menus (qui sont par défaut aux valeurs que moi, j'utilise : 30 images / secondes, Matroska) et celles des deux réglettes `début/fin`, et on construit la commande FFmpeg (et le nom du fichier de destination) avec tout ça :

```python
out_file = '{}_{}-{}_{}fps.{}'.format(filename,
                                      str(start_time),
                                      str(end_time),
                                      self.frame_rate,
                                      self.file_format)
command = [FFMPEG, '-y',
           '-i', filename,
           '-r', self.frame_rate,
           '-strict', '-2',
           '-c:v', 'libx264',
           '-ss', start_time,
           '-to', end_time,
           out_file]
```

Et on envoie tout ça à une classe qui

- lance le process (en asynchrone, et rend la main) ;
- vérifie toutes les secondes qu'il n'est pas fini ;
- avertit l'utilisateur quand il est fini.

Le fichier de destination est sauvé dans le répertoire du fichier d'origine.

Au chargement d'un fichier multimedia – au lancement du programme ou après – c'est le même processus qui gère la génération de la forme d'ondes, qui s'affiche quand elle est prête (pour un gros fichier, ça peut prendre plusieurs longues secondes) sans ralentir l'interface.

### Êtes-vous sûr de ne pas vouloir arrêter le processus d'annulation ?

Du coup, comme je ne lis pas la sortie de FFmpeg (je vérifie juste les codes de retour) je suis en asynchrone, yay, mais je perds la possibilité d'informer l'utilisateur de l'avancement du travail en cours, bouh ; juste un bidule qui pulse pour indiquer qu'il se passe quelque chose. Notez que FFmpeg lui-même ne le fait pas, mais il indique quelle image il est en train de traiter, or moi, j'ai plein de moyens de savoir combien d'images fait mon fichier, à commencer par Mediainfo. Eh. Dommage.

Pour arrêter un job FFmpeg qui prendrait trop de temps, il faut quitter le programme. Je ne suis pas en train de décrire un choix d'implémentation là, hein, mais la conséquence  du choix de ne rien implémenter du tout pour le moment.

Chez moi, ça marche (tm) :) mais je vais quand même, c'est sûr, à un moment, faire une entrée de menu avec un raccourci clavier pour envoyer `q` au process FFmpeg en cours (oui, il écoute stdin – à la fois des pipes nommés pour le streaming ET le clavier pour les commandes – et s'exprime exclusivement sur stderr, il est spé' FFmpeg) j’attends de voir un peu comment ça se passe au niveau ergonomique.

### Genre de style d'UI

Afficher une image, l'étirer sur toute la largeur de la fenêtre principale, puis la re-dimensionner quand change la taille de cette dernière, est un processus assez lourd, qui implique une gestion relativement fine des événements, si on veut faire ça en utilisant les classes et méthodes de Gtk lui-même (`Gtk.image`, `Gtk.Pixbuf`, `Gtk.DrawingArea`, `Gtk.ScrolledWindow` etc.) c'est ainsi que marchait [la première version](https://bitbucket.org/yphil/fwomaj/src/0dec2d5e1d0e40e2186bb1c9b103328659fddfe1/derusher.py?fileviewer=file-view-default) en Python2/Gtk2, mais le principe est le même.

Mais [Gtk3 intègre **CSS3**](https://developer.gnome.org/gtk3/stable/chap-css-overview.html), ce qui a permis de remplacer ce genre de machin :

```python
    def on_resize(self, widget, event, window):
        allocation = widget.get_allocation()
        if self.temp_height != allocation.height or self.temp_width != allocation.width:
            self.temp_height = allocation.height
            self.temp_width = allocation.width
            pixbuf = self.waveform_image.get_pixbuf().scale_simple(allocation.width,
                                                                   allocation.height,
                                                                   gtk.gdk.INTERP_NEAREST)
            widget.set_from_pixbuf(pixbuf)
```

Oui oui, c'est bien ça, à _chaque_ événement (cette fonction est un callback sur l'événement `on_resize` de la fenêtre, donc des centaines de fois par seconde, dans la joie) on re-crée un pixbuf en attrapant celui de l'image en place, on le retaille aux dimensions `XY` de la fenêtre, et on le remet dans l'image, qu'on remet dans le widget, ouf :/ c'est comme ça qu'on fait, au centre hippique PyGtk.

Par quelque chose comme ça:

```python
CSS = '#WBox {background-size:100% 100%;background-image: url("{}");}'.format(uri)
style_provider.load_from_data(CSS)
```

Quand j'ai percuté (je suis développeur web, moi, à la base) genre sous la douche, je me suis dit c'est pas possible, ça peut pas être si simple, ça a marché au _premier_ essai.

### Mediainfo

...Est le meilleur outil d'analyse de fichiers multimédia, oui du monde, même sans les mains, point final. J'avais commencé à implémenter une usine à mazout en utilisant [les fonctions de "découverte" de GStreamer](https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-libs/html/gst-plugins-base-libs-gstdiscoverer.html), pour éviter une dépendance, mais sa souplesse et son intelligence relative étaient ridicules face aux capacités d'introspection de [Mediainfo](https://packages.debian.org/jessie/mediainfo).

### Bugs

Quoi ? Naaan :) Si. On commence par les petits.

#### Blanker (Gtk)

Quand on passe en plein écran, les contrôles disparaissent, ainsi qu'il est d'usage. Ce n'est pas du vrai plein écran d'ailleurs, au sens où on l'entend : Le gestionnaire de fenêtre n'est pas averti, pour commencer, enfin je crois, je maximise juste la `Gtk.DrawingArea` où j'affiche les pixels qu'envoie le `pipeline` de GStreamer.

Quand on bouge la souris, ces contrôles réapparaissent, toujours selon les protocoles en vigueur. Mais je n'ai pas réussi (j'y ai passé toute une soirée !) à démarrer un timer à ce moment, pour qu'au bout de, mettons 10 secondes d'inactivité de la souris ou du clavier, lesdits contrôles disparaissent à nouveau. Je parie que c'est une tarte à la crème, un classique du développement d'interfaces graphiques, et que quelqu'un ici va bien nous trouver une solution :)

#### Rendu (GStreamer)

Ça n'a bien sur pas d'incidence sur le fichier produit, mais le rendu vidéo est bordé d'un pixel de couleur, variable, comme une sorte de somme spectrale de l'image en cours, et ça bouzille le noir qui est d'usage autour de tout film décent. Mon implémentation est bancale, clairement. Je _déteste_ ce bug minuscule et en même temps énorme, il me donne l'impression de ne rien contrôler du tout.

C'est évidemment un bout de code que j'ai copié collé de je ne sais plus d'où, car c'est _pas du tout_ comme s'il existait ne serait-ce qu'une page dans tous les interfubles qui dit "bon, voilà, GStreamer 1.0 est sorti, même gst-launch a été renommé gst-launch-1.0 pour qu'on comprenne bien que plus rien n'est compatible, et donc voilà quelques exemples d'usage en Python3 / Gtk3 oh rien du tout, juste une implémentation de base.

On peut faire [plein de choses avec GStreamer](http://wiki.oz9aec.net/index.php/Gstreamer_cheat_sheet). C'est un langage de pipes vidéo assez fascinant, qu'on prototype à l'aide de l'utilitaire [gst-launch](https://packages.debian.org/sid/gstreamer1.0-tools), pour construire des "tuyaux" d'images qui bougent:

```sh
gst-launch-1.0 videotestsrc pattern=1 ! video/x-raw,format=AYUV,framerate=\(fraction\)10/1,width=100,height=100 ! videobox border-alpha=0 top=-70 bottom=-70 right=-220 ! videomixer name=mix sink_0::alpha=0.7 sink_1::alpha=0.5 ! videoconvert ! xvimagesink videotestsrc ! video/x-raw,format=AYUV,framerate=\(fraction\)5/1,width=320,height=240 ! mix.
```




Mais GStreamer est si complexe que sa documentation plane dans les limbes...

Du coup GStreamer est... magique :

![GStreamer](https://linuxfr.org/images/historique/images_perdues/fwomaj-0-3-videos-a-la-coupe-au-rayon-frais-JVEIcZ13DXEt.png)

Sur cette image prise en plein écran (la sublime ouverture de « Michael Clayton  », T. Gilroy 2007) on remarque deux choses : l’agaçante bande de couleur évoquée plus haut (deux pixels mauves à gauches, et une sorte de frange  verte intermittente d'un pixel tout autour) et les sous-titres... attend, quoi ?

#### Sous-titres (anti-bug, GStreamer)

Oui. GStreamer, sans que j'ai fait _quoi que ce soit_ pour ça, m'extrait avec force diligence les sous-titres du container du film en question (Matroska, en l'occurence, qui est réputé pour ça entre autres) et me les affiche, synchronisés, en Times, c'est trop chou, mais, heu, comment je contrôle ça ? Une fonction (et pas n'importe quoi, la gestion des sous-titres je voulais même pas seulement y _penser_) qui tombe en marche par accident, c'est un bug aussi, non ? GStreamer 0.10 ne faisait **pas ça** ; on va pas se plaindre trop fort non plus, allez.

#### États (non-bug, Python)

Mon usage de Fwomaj est le suivant : Je double-clique sur un fichier vidéo, je le regarde ou je l'édite, bref je fais ce que j'ai à faire avec, et je le ferme. Et ça, ça marche très bien. Mais d'autres peuvent vouloir l'utiliser différemment. C'est dans cet esprit que j'ai codé un « Open File » dont je me sers _jamais_, par exemple ; quand on a fini d'encoder une vidéo, l'état de Fwomaj n'est pas vraiment net-net. Si ça se trouve, ça marche très bien, mais je n'ai pas testé cet usage (ouvrir un fichier, et l'encoder plein de fois avec plein de paramètres différents, puis en ouvrir un autre, dans la même instance, tiens, rien que le fait d'écraser le fichier — c'est ce qu'on fait maintenant : mêmes paramètres, même début, même fin = même nom de fichier, on écrase — c'est bien gentil, ça marche, mais si j'étais responsable Debian (par exemple) je ne laisserais clairement pas passer Fwomaj dans les dépôts officiels, à cause de ce genre de détail.

Ça vient de ma façon de coder, procédurale sinon déclarative (on dit aussi « travail de goret » par chez moi) et c'est vrai que je voulais juste un lecteur pour visionner, couper mes rushes et en harmoniser les caractéristiques pour pouvoir les monter sans devenir _encore plus dingue_ (intermède : si tu achètes un SONY Alpha Nex5 au Japon ou aux USA, il fait des vidéos en 30 et 60 images/secondes. Si tu as la mauvaise idée de l'acheter en Europe, c'est [**_une autre machine_**](http://www.dpreview.com/forums/thread/3174771), modifiée en dur pour produire des vidéos en 25/50 images/secondes, et je ne ferai pas d'autre commentaire là-dessus, c'est mieux pour la tenue de cette dépêche) et que ce lecteur, je l'ai, super.

Maintenant, si j'avais un vœu pour ce petit bout de logiciel au-delà de ce qu'il m'apporte maintenant, c'est qu'un de ces gourous qui traînent par ici (nan mais vous pouvez filer en douce, trop tard hein, on _lit_ vos commentaires, vous croyez quoi ?) se penche sur son berceau pour en ré-usiner (tiens, ça claque encore mieux en français dis-donc) le code _en profondeur_, oui. Fwomaj 0.3, en l'état, est un prototype. Qui marche pas mal, mais un prototype quand même. Il me semble que c'était ça l'idée de Python, non, de bricoler vite fait des trucs qui marchottent pas mal ? Bon, ben c'est réussi, merci les gars :)

## Installation

En pratique, je n'ai testé Fwomaj 0.3 que sous Ubuntu 14.04 et 16.04, avec les environnements de bureau Unity et XFCE.

### Debian

```sh
wget https://bitbucket.org/yphil/fwomaj/downloads/fwomaj_0.3_all.deb && sudo gdebi fwomaj_0.3_all.deb
```




Oui, je n'ai pas trouvé comment installer les dépendances d'un paquet (duh) avec `dpgk`..?

Ou on peut aussi construire soi-même le paquet. Ne pas se laisser abuser par le répertoire `./debian`, Fwomaj est juste un script Python de 800 lignes et une icône.

```sh
git clone https://bitbucket.org/yphil/fwomaj.git
cd fwomaj
./build_package.sh
sudo gdebi -i ../fwomaj_0.3_all.deb
```

### Et les autres systèmes?

Pour les autres Linux, ça doit être assez trivial. À minima, le script peut être lancé tel quel sur une machine où sont installées les quatre dépendances, il n'aura juste pas d'icône. Les outils qui servent à créer des paquets pour d'autres distributions n'auront probablement aucun mal à se débrouiller avec les éléments présents :

- exécutable ;
- page de man (minimale, exigée par [Lintian](https://lintian.debian.org/)) ;
- icônes ;
- journal (changelog) ;
- et éventuellement les fichiers de contrôle/description, tout ça n'est guère spécifique.

Pour les autre systèmes, honnêtement, je ne sais pas. Bien sûr, on doit pouvoir faire tourner Gtk3 sous Windows, FFmpeg aussi mais GStreamer, je suis moins sûr.

En tout cas, les spécificités du système d'exploitation sont abstraites autant que faire se peut :

```python
unique_file = os.path.join(tempfile.gettempdir(), 'fwomaj-{}.png'.format(time.time()))
```

Devrait marcher partout, en principe.

Il parait qu'il y a Python sous AmigaOS, maintenant :|

J'utilise Fwomaj depuis une petite quinzaine comme lecteur principal, j'ai préparé [une demi-douzaine d'épisodes](https://yphil.gitlab.io/blog/2016/07/07/Filmer-monter-encoder-diffuser-Why-Phil/) avec, et encodé un grand nombre de fichiers d'origines diverses, ça marche bien.

Allez-y, testez 😉
