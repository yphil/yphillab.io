---
title: List i3 keyboard shortcuts
date: 2022-10-17 14:30:39
tags:
- i3
- python
---

## It's gonna get all nerdy in here 👓 

Yesterday afternoon I had a spare moment and decided to waste it by scratching a *tiny* itch ; So I went ahead and wrote a script to parse the i3 config file, extract `keysym` bindings infdormation (yes, even those nested into modes) and present it in a useful and timely manner.

![i3 CKB Shortcuts](/images/i3Shortcuts.jpg)

It could be better ; What I'd like is something close to [which-key](https://elpa.gnu.org/packages/which-key.html), that displays available key combinations along your typing, that is, when you type `$mod+a` to launch an application, it presents you with the list of already bound keys ; But this is just a plain list, presented in the most ergonomic way possible.

So now on my machines, both `$mod+Shift+k` and `k` in any mode shoots up the window.

Technically, it is a Python3 script that reads and parse `~/.config/i3/config` and, depending on the CLI arguments, outputs them to the console `stdout` or as a [Pango markup](https://docs.gtk.org/Pango/pango_markup.html) formatted page, that can then in turn be read by a browser, or something like [zenity](https://help.gnome.org/users/zenity/stable/).

Currently, it's reading my modes because of the specific formatting of [my own config file](https://framagit.org/yphil/dotfiles/-/blob/master/config/i3/config#L158) :

``` c++ ~/.config/i3/config
# MODE:
bindsym $mod+a mode "$app_launch_mode"
mode "$app_launch_mode" {
    bindsym a exec ardour, mode "default"
	# (...)
}

# MODE:
bindsym $mod+b mode "$dummy_launch_mode"
mode "$app_launch_mode" {
    bindsym a exec somethingelse, mode "default"
	# (...)
}
``` 

So YMMV. This script *will* be universal in good time. Think about it really hard, it's not that easy if you put your `exec`s in vars. Yes, I told you it wasn't trivial.

``` shell
# px-i3-keyboard-shortcuts.py -h
usage: px-i3-keyboard-shortcuts.py [-h] [-t FORMATTING] [-o OUTTYPE] [-f OUTFILE]

options:
  -h, --help            show this help message and exit
  -t FORMATTING, --formatting FORMATTING
                        Format of the output: html (default) / text
  -o OUTTYPE, --outtype OUTTYPE
                        Output type: stdout (default) / file / full (both)
  -f OUTFILE, --outfile OUTFILE
                        Temp. out file PATH (defaults to /tmp/.i3-kb-bindings.html)

```

Normally I'd just feed the page to the default browser, but `xdg-open` is currently broken in Firefox snap 😕 

Source [here](https://framagit.org/yphil/dotfiles/-/blob/master/.scripts/px-i3-keyboard-shortcuts.py).

## In i3

``` c++ ~/.config/i3/config
set $i3_keyboard_shortcuts "~/.scripts/px-i3-keyboard-shortcuts.py -t html -o file && zenity --text-info --title='i3 Keyboard Shortcuts' --width=640 --height=800 --html --window-icon=info --filename=/tmp/.i3-kb-bindings.html"

bindsym $mod+Shift+k exec $i3_keyboard_shortcuts
```

But also in all the modes (that is, press `k` when you're in a mode and forgot the key you wanted to type)

``` shell ~/.config/i3/config
bindsym $mod+z mode "$system_mode"
mode "$system_mode" {
    bindsym e exec --no-startup-id $exit_script logout, mode "default"
    bindsym h exec --no-startup-id ~/.scripts/px-hdmi-reset.sh 2, mode "default"
    bindsym Shift+h exec --no-startup-id $exit_script hibernate, mode "default"
    bindsym k exec --no-startup-id $i3_keyboard_shortcuts # <= HERE
    bindsym l exec --no-startup-id $exit_script lock, mode "default"
    bindsym r restart, mode "default"
    bindsym Shift+r exec --no-startup-id $exit_script reboot, mode "default"
    bindsym s exec --no-startup-id $exit_script suspend, mode "default"
    bindsym Shift+s exec --no-startup-id $exit_script shutdown, mode "default"

    bindsym Return mode "default"
    bindsym Escape mode "default"
}
``` 

That's it seen you next time have a nice week 🤖 
